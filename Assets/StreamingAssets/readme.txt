This explains all of the files contained within the streamingAssets folder

In this directory, there are a couple of built in maps, but new maps that are saved go to the game's permanent storage directory (done this way to try to avoid some access errors with certain permission levels).  That directory is normally c:\Users\(User Name)\AppData\LocalLow\UnbridledHedonism\UnbridledHedonism\Maps on windows computers.

There are female names.txt,  male names.txt and last names.txt in this directory, and names can simply be added to them to be added to the list of available random names.   It's worth noting that they would be overwritten by new versions, so make a backup of them if you're attached to your list.  

Last saved is just a copy of the last saved character, which you can generally just ignore, unless you're using it to more easily make a named character.



Pictures


In the pictures folder you can supply pictures for various names.  It's based on first names.  If there are no pictures for a name, it won't display anything, and will look as it did before.    By default a character will use their name as what picture they'll try to use, but you can change that when you're modifying a character with the in-game editor.   There are a few variations of pictures allowed, by using a - with the type after it.  I.e. Beth-Horny.
This is the priority of the picture types, if there is no picture for that condition, it will just skip that step.  I.e. if a character is being eaten and horny but there is no being eaten picture it will use the horny one.





--Dead--
-DeadCV : Prey digested in balls. 
-DeadUB	 : Prey digested in womb. 
-Dead : This will display if the character is dead.  Note that this and eaten can be overwritten by the Pred's belly sprite if that setting is on.  
--Eaten--
-EatenUnwillingNude : This will display if the nude character inside Pred and unwillingly eaten.
-EatenUnwillingUnderwear : This will display if the character with underwear inside Pred and unwillingly eaten.
-EatenUnwilling :  This will display if the character inside Pred and unwillingly eaten.
-EatenNude : This will display if the nude character inside Pred.
-EatenUnderwear : This will display if the character with underwear inside Pred.
-EatenHealthy : This will display if the character inside Pred and at more than 60% health.  Could be used as an endo image, or as a lighter digestion image.  
-EatenWeakened : Prey health between 30% and 60%.
-Eaten : This will display if the character inside Pred.
--Disposal--
-DisposalCV : This will display the Disposal from CockVore
-DisposalUB : This will display the Disposal from Unbirdhing
-Disposal : This will display the Disposal from belly(guts).
--Full Absorbing--
-FullBothAbsorbingNude : This will display the prey in belly and nude Pred while absorbing.
-FullBothAbsorbingUnderwear : This will display the prey in belly and Pred with underwear while absorbing.
-FullBothAbsorbing : This will display the prey both in belly and balls and Pred while absorbing.
-FullBallsAbsorbingNude : This will display the prey in balls and nude Pred while absorbing.
-FullBallsAbsorbingUnderwear :  This will display the prey in balls and Pred with underwear while absorbing.
-FullBallsAbsorbing : This will display the prey in balls and Pred while absorbing.
-FullAbsorbingNude : This will display the prey in belly and nude Pred while absorbing.
-FullAbsorbingUnderwear : This will display the prey in belly and Pred with underwear while absorbing.
-FullAbsorbing : This will display the prey in belly and Pred while absorbing.
--Full Horny--
-FullHornyBothNude :  Prey in both belly and balls. Nude and Horny Pred.
-FullHornyBothUnderwear : Prey in both belly and balls. Horny Pred with underwear.
-FullHornyBoth : Prey in both belly and balls. Horny Pred.
-FullHornyBallsNude : Prey in balls only. Nude and Horny Pred.
-FullHornyBallsUnderwear : Prey in balls only. Horny Pred with underwear.
-FullHornyBalls : Prey in balls only. Horny Pred.
-FullHornyNude or -HornyFullNude : Prey in belly only. Nude and Horny Pred.
-FullHornyUnderwear or -HornyFullUnderwear : Prey in belly only. Horny Pred with underwear.
-FullHorny or -HornyFull : This will display if both Horny and Full conditions are met.
--Full--
-FullBothNude : Prey in both belly and balls. Nude Pred.
-FullBothUnderwear : Prey in both belly and balls. Pred with underwear.
-FullBoth : Prey in both belly and balls.
-FullBallsNude : Prey in balls. Nude Pred.
-FullBallsUnderwear : Prey in balls. Pred with underwear.
-FullBalls : Prey in balls only.
-FullNude : Prey in belly. Nude Pred.
-FullUnderwear : Prey in belly. Pred with underwear.
-Full : This will display if the character has any sort of prey, regardless of the location.
--Horny--
-HornyNude : This will display if nude character is above 80% horniness (which is usually the threshold when they start seeking out something to help with their horniness).
-HornyUnderwear : This will display if the character with underwear is above 80% horniness.
-Horny : This will display if the character is above 80% horniness.
--Hungry--
-hungryNude : This will display if nude character is above 60% hunger.  
-hungryUnderwear : This will display if the character with underwear is above 60% hunger.  
-Hungry : This will display if the character is above 60% hunger.  
--Clothes Status--
-Nude : This will display nude character.
-Underwear : This will display the character with underwear.
(Blank) : This will display if none of the above are true, or if they don't have pictures.
The Auri pictures provide a demonstration for the way the files should be named.  
There's not really a size limit, though the pictures will auto scale to the available space.  The player can set the picture size, which defaults to 200 x 200 pixels at a 1920x1080 resolution, though it can be set all the way up to 500x500.   
It can read both .png and .jpg files, though .png supports transparency and .jpg doesn't, which may affect what type you choose.  




Interaction text editing 

There's several files for the interactions, they're technically tab separated spreadsheets, but the spreadsheet program I use doesn't recognize that ending as a separate thing so they end as .csv files, though the game will also accept them as .tsv files.  

If you're having trouble getting the file to open /save in the right format, you can put the spreadsheet up on google sheets, then use save to file as .tsv and move it to the right directory (you don't need to rename it to a .csv, but you can if you like.),  You can also use LibreOffice like I do with a little fiddling to get the filters right. CSVed ( https://csved.sjfrancke.nl/index.html )looks like a decent editing option as well, and doesn't force you to install it, though you'll want to go into the options, in the double quotes section, and set the 'set another quote character' to something like @ , so that it doesn't try to add extra quotes to 'fix' the text.  

After the interactions are read in game, it will generate a summary.txt file that tells you the total number of interactions for all types, and what interactions in the files don't have a matching interaction in game, and what interactions are missing texts, if any.  

So, a basic explanation for interaction texts.  The game looks to get text when an interaction happens, and then it will randomly pick from any of the highest priority messages that have a true conditional.   In some cases i've explicitly spelled out conditionals, in order cases I've just used the priority system to filter the messages.  Like for PreyWait, there's a message with a priority of 4 that's true when the person is dead, so it will always happen if the prey is dead, and otherwise it will pick from the other messages.  
Feel free to add additional messages in existing categories (just add a row and use the interaction name.  

There are also Yes and No for some interactions, Yes means the action succeeded, No means it failed or was rejected

There are a bunch of replacement tags you can use to refer to certain variables

On any of these, Actor can be substituted for target.  Actor is the one performing the action, Target is the one the action is used on.  (Self actions can use either, though I think it makes more sense to use Actor).  Also worth noting that these are not case sensitive, though I usually will use that for easier reading.
Can Put a ! in front of these to make it capitalized.  Like [!ActorHis] would become 'His' for a male.  
[ActorName] The Actor's name
These replace the pronoun with the gender appropriate pronoun (they're male because his and him both convert to her)
[ActorHeIs] becomes 'he is' for males, 'she is' for females, and 'they are' for other
[ActorHeHas]
[ActorHis]
[ActorHim]
[ActorHimself]
[ActorHe]
[ActorBastard]

[SIfActorSingular] - Adds an s if the actor is using male or female pronouns, but leaves it off for neutral (so things like 'he sees' 'they see' work correctly)
[ESIfActorSingular]
[IESIfActorSingular]

[ActorBoobSize] - Gets the descriptive word for the actor's boob size
[ActorDickSize]
[ActorBallSize]

[PredatorOfActor] - Gets the first name of the Actor's direct predator - avoid using without a conditional that makes sure they are prey, or an event where it's required
[ActorLocationInPred] - Gets the name of the location the Actor is in, i.e. balls, stomach.  

[Breasts] - Gets a synonym for breasts (i.e. boobs, tits)
[Dick] - Gets a synonym for Dick (i.e. cock, member)
[Pussy] - Gets a synonym for Pussa (i.e. vagina, slit)
[Pussies] Gets a plural synonym for pussy (i.e. slits, nethers)
[Balls] Get a synonym for Balls (i.e. scrotum, nuts)
[Clit] Gets a synonym for Clit (i.e. g-spot, button)
[Clits] Gets a plural synonym for Clit (i.e. g-spots, buttons)
[Anus] Gets a synonym for Anus (i.e. ass, rear)

These pull from the character's info page (i.e. when you edit appearance)
[TargetEyeColor]
[TargetHairColor]
[TargetHairLength]
[TargetHairStyle]
[ShoulderAdjective]
[WaistAdjective]



For the Conditionals, there's a few things that I left in their code version as they're rarely used, though that may change in the future.  If you're unsure how to use these, you can just write in english and I'll convert it (on the online spreadsheet, or sent to me, not into your local copy).  Also, && is 'and', and || is 'or'.  Also worth noting that these are not case sensitive, though I usually will use that for easier reading

[ActorFullyClothed] - if the actor is fully clothed
[ActorInUnderwear] - if the actor is wearing only underwear
[ActorNude] - if the actor is wearing no clothes
[ActorHasBoobs] - if the actor has boobs
[ActorHasDick] - if the actor has a dick
[ActorHasVagina] - if the actor has a vagina
[ActorPreySize] - The size of the Actor's prey - Right now people count as 1 unit, and when being absorbed they shrink.  
[ActorDigestsPrey] - Worded slightly oddly because it's legacy - but Whether the actor is currently digesting the target
[VoreKnowledge] - Whether vore is considered to be common knowledge (i.e. the game settings toggle), was designed to make people react differently, though individual characters will still be surprised no matter how many times they see it.
[ActorWillingPrey] - Whether the actor is willing prey
[ActorCanVore] - Whether the actor is capable of performing vore
[TargetIsActorPrey] - If the target is a direct prey of the actor (i.e. not free, or a prey of prey)
[TileHasBed] - If the current tile has a bed in it.
[TileHasFood] - If the current tile has food in it (i.e. the cafeteria)
[TileHasGym]
[TileHasLibrary]
[TileHasShower]
[TileHasBathroom] 
[TileHasNurseOffice]

[TileIsActorsRoom]		Tile is the actors room
[TileIsTargetsRoom]		Tile is the targets room
[OthersOnTile]			There are other characters on the same tile with actor

[ActorLikesTargetGender] - Whether the actor finds the target's gender romantically appealing or not
[TargetLikesActorGender] - the flip
[TargetFriendshipTowardActor] - What the target thinks of the actor
[ActorFriendshipTowardTarget] - What the actor thinks of the target
[TargetRomanticTowardActor]
[ActorRomanticTowardTarget]
[ActorHasLivingBellyPrey] If the Actorhas at least one living prey within their belly (only currently counts direct prey, not prey of prey)
[ActorHasLivingBallsPrey]
[TargetHasLivingBellyPrey]
[TargetHasLivingBallsPrey]
[ActorIsBeingEaten] If the actor is within another person, is true even while being swallowed.  
[TargetIsBeingEaten]

There's a set for determining prey location, they should be mostly self explanatory.  
[TargetIsInActorStomach] - Returns true if the Target is in the Actor's stomach, returns false if they're in another location, or simply not in the Actor at all.  
[TargetIsInActorBalls]
[TargetIsInActorBowels]
[TargetIsInActorWomb]
[ActorIsInTargetStomach]
[ActorIsInTargetBalls]
[ActorIsInTargetBowels]
[ActorIsInTargetWomb]

[TargetOralVored] Returns true by the current location of the target in whoever ate them.  It can be used to tell what location the target is in, note that this isn't in reference to the Actor, only the Target's location
[TargetUnbirthed]
[TargetCockVored]
[TargetAnalVored]
[ActorOralVored]
[ActorUnbirthed]
[ActorCockVored]
[ActorAnalVored]

[ActorOfferedSelf] Whether the actor was the one who offered themselves to the predator
[TargetOfferedSelf]



Game Settings related
[DisposalEnabled]
[DisposalCockEnabled]
[DisposalBonesEnabled]
[DisposalUnbirthEnabled]
[BodyWeightGain]
[BoobWeightGain]

Sex related
[BeingPenetrated]
[Scissoring]
[Penetrating]




There are some more advanced commands you can use in the conditional area, such as 
s.Actor.HasTrait("NameOfTrait") - returns true if the actor has the given trait or quirk, it's case insensitive, but the quotes are required.  s.Target.HasTrait("OtherTrait") for the target of the interaction.

If you have a conditional that's invalid - i.e. spelling mistake or using variables that don't exist, it will give you an in-game warning for the first one that happens during a session, and drop that and any others that happen into a 'compileErrors.txt', and flag them so that they don't show up during that game.   In the future there will probably be a way to check this in advance without waiting for it to show up in game, but that didn't make it into this first version.   

To use custom variables, i.e. the demi-fox's tail color, a slightly verbose [ActorCustom-Tail Color] is used, note that the variable name is case sensitive and requires any spaces that are in the variable name.   If it fails for whatever reason (wrong variable, or that character doesn't have that part), the [ActorCustom-Tail Color] will simply be left in the final message.  

Race Basics

You can set the Actor Race and the Target Race for any texts, to control who has access to that text.  Any character with specific dialogue is considered a race in game-terms.  

Characters have a stack of races, I.e. Auri is Auri / Demi-Fox / Demi-human / Human(Base), it falls through if there's no valid options at that level. A Demi-fox to Human interaction used with 0 priority will be picked before any Human to human interactions of that type.   You can set the chance of fallthrough, for example, if you add one text for an action that has a lot of base texts, you can make it so that there's a 20% chance of that text happening, otherwise it falls through.  That allows you to make some unique text without making that interaction appear all of the time or duplicating some of the base stuff.    

Interactions will try to match both Actor and target, if they can't, it prefers matching the actor, then the target.  If it can't match any, it starts looking up the tree.  I.e. If you have a demi-cat to demi-fox interaction, it could theoretically match with Auri as a target, but only if there's no specific demi-cat to Auri interaction, or if it's set to possibly fall through.  

Allowing multiple races to use some of the same interactions should be done through the parent system if possible, but if you can't, you can use multiple races for actor or target by combining races with & .  For example, if you wanted an interaction to match with demi-foxes and demi-cats (maybe something to do with races that are often predators), you could do ' Demi-Fox&Demi-Cat ' in the actor field.  

The actual files don't affect the way the way interactions are read or processed, it's mainly categorizing them for convienience.  If you've added on to one of the base races, you might consider putting the new ones in a separate file, like 'Demi-Fox Extras', so that your changes aren't overwritten by a new version if you're unzipping replacements into the same place.  


Races

The actual race directory is relatively simple.  Creating a text file will create a race of that name, and it needs 3 properties
The parent race is the race that that race is considered a subset of.  For example Demi-Fox has Demi-human as a parent, which has Human as a parent.  That means any human or demi-human interaction text can be used by the Fox, though it tries to use the closest match, see the interactions bit above for more details.  Races intended to be unique (if any exist) should still specify human as a parent, so that messages they don't supply text for still have some text if they're accidentally missed. 
Selectable is simply whether the race appears as a chooseable race.   Demi-human is an example of a non-selectable race, as it's just there to pad out interactions (the sample interaction of a demi-human meeting another demi-human).  It's not really a race because anything that would be a demi-human should be a more specific example, i.e. demi-fox or demi-cat.  Also, named characters should have this disabled.
Then named char simply tells the game whether this race is a specific person, letting it know whether it should look for it in the characters directory.  

You can also specify what options are randomly available as choices, you can see the humans.txt file for examples of how to use it, and some comments (and you can change them if you want to).  Specifying Masculine or Feminine applies means that that only applies to feminine or masculine characters, while leaving it out makes it apply to everyone.  If you specify one you should specify the other specifically, as one that's not specifying would overwrite what came before.  I.e. If you specified Hair, Feminine, and then general hair, the general would overwrite the feminine.  The sizes are based off of the in-game values, so check in-game for descriptions of say breast-size if you're not sure.  You can also specify traits and quirks using the same formatting, and all members of that race (or the restricted masculine / feminine) will always have that trait/quirk, assigned for free.  Formatting is the Same, so for instance 'Quirks, Forgiving, Rescuer'

For child races, you can specify things there, and they effectively go in order from general to specific.  I.e. the hair color specification in demi-foxes overwrites the one in human, so demi-foxes would always use that, which adds some more distinct colors not normally seen in human hair (like orange or white), while also removing auburn colored hair as a possibility since it's not listed there.  

For named characters, you can either overwrite some of the description text (like tail color for Auri I think is never used because her text just calls it orange), or specify it for the character.  If you click save on a character, it will toss out a LastSaved.txt into the streaming assets folder, that you can use to easily make that character into a named character if desired (renaming it and copying it).  Be careful editing it manually, because if you delete quotation marks or fields it may behave in an unexpected manner, but as long as you avoid that you can edit it without issues.   Generally you won't want to bother adding the randomized set like you would do for normal races because the use of a custom character overwrites anything there.  


Race Stats

You can also create preset random stat ranges for each race individually, including separate settings for masculine/male and feminine/female characters. The settings get applied to any character set to controlled random.

It reads configuration files from a new folder "RandomStats" in the Races directory. A demo file for Human is included.

Min/Max values can also have an optional abnormality chance set. This is a % chance that the character will have an abnormality in that stat that may be higher or lower than the set ranges.

Example: Charisma, 0.3, 0.7, 0.1
(Min 0.3, Max 0.7 and a 10% chance of landing outside those ranges.)


Tile graphics
Tiles now read their graphics from files, instead of being unchangeable.  Basically you can take each type, and change the way it looks.   Since there are several types that currently share the same flooring type, I went ahead and duplicated those so they can be changed individually if desired, though they still share by default.  The files are in the streamingassets/Tiles directory.
The types are
    IndoorHallway,
    Grass,
    OutdoorPath,
    DormRoom,
    Cafeteria,
    Shower,
    NurseOffice,
    Bathroom,
    Gym,
    Library,
It looks for those names, as a .png or .jpg and reads them in.  If one of those files is missing, it will simply show up in-game as a white dot.
You can also change the objects,
Those are Bed, NurseBed, and toilet, books, and gymObject.  
You can set the scale of the objects in the sizes.txt, as they will otherwise try to be tile sized.  If the object is missing it will simply draw nothing.  This allows you to draw objects into the tile itself if you want.  Note that for objects you'll want to use .png for the image as .jpg doesn't support transparency, so the object would basically overwrite the tile.  
