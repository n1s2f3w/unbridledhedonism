Parent Race: Demi-Human
Selectable: True
Named Character: False

Hair Color, black, brown, dark brown, sandy yellow, white, gray

Height, Feminine, 48, 60
Height, Masculine, 50, 65
Weight, .65, 1.10

Custom, Tail Color, black, brown, dark brown, pink, white, gray
Custom, Tail Desc, long, very long, short, thick