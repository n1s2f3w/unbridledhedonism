Parent Race: none
Selectable: True
Named Character: False

Hair Color, brown, black, blonde, auburn, red
//Some colors are used more than once to more them more likely to be picked
Eye Color, brown, hazel, blue, green, brown, hazel, blue, green, gray, amber
Shoulder Description, wide, broad, narrow
Hip Description, wide, broad, narrow
Hair Length, Feminine, short, chin level, shoulder length, mid-back length
Hair Length, Masculine, short, medium
Hair Style, Feminine, bangs, braids, french braided, ponytail-styled, surfer style
Hair Style, Masculine, crew cut, flattop, ivy league, taper cut, slicked-back, undercut
//Heights in inches because that's what they are internally
Height, Feminine, 54, 74
Height, Masculine, 58, 77
//Generates between 85% and 115% of the 'normal' weight for their height
Weight, .85, 1.15

Breast Size, Feminine, .6, 5
//Masculine breast size is for sexes with breasts but not feminine
Breast Size, Masculine, .2, 3

Dick Size, Masculine, 0, 5
Dick Size, Feminine, 0, 4
//Ball sizes are tied to the dick size, defaults to 85-115%
Ball Size, Masculine, .85, 1.15
Ball Size, Feminine, .85, 1.15

