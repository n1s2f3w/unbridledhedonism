Parent Race: Human
Selectable: True
Named Character: False

Eye Color, red, golden, brown, red, red, red
Height, Feminine, 136, 228
Height, Masculine, 144, 240
//Generates between 85% and 115% of the 'normal' weight for their height
Weight, .85, 1.15

Custom, Skin Color, brown, beige, crimson, brown, brown, brown, beige, black, white
Custom, Tooth Size, average, sharp, long, short, worn-down
Custom, Tongue Size, average, long, short, massive