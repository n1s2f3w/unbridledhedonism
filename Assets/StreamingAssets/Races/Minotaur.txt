Parent Race: Demi-Human
Selectable: True
Named Character: False

Hair Color, black, brown, blonde, orange, white, red

Custom, Leg Fur Color, black, brown, blonde, orange, white, red, yellow, silver, gold

Custom, Horn Type, straight, curved, short

Custom, Horn Orientation, upward-facing, sideways-facing, downwards-facing

Height, Feminine, 72, 80
Height, Masculine, 72, 80

Weight, .85, 1.15

Breast Size, Feminine, 2.5, 4.5
