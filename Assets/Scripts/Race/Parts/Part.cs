﻿using OdinSerializer;
using Assets.Scripts.TextGeneration;

namespace Assets.Scripts.Race.Parts
{
    class Part
    {
        [OdinSerialize]
        internal int PartDifficulty = 0;

        [OdinSerialize]
        internal BodyPartType Type;

        public Part(int partDifficulty, BodyPartType type)
        {
            PartDifficulty = partDifficulty;
            Type = type;
        }

        internal BodyPartDescriptionType GetBPD(BodyPartDescription description)
        {
            return BodyPartLinker.GetType(Type, description);
        }

        internal string GetDescription(Person target)
        {
            return MessageManager.GetText(
                new Interaction(target, target, true, InteractionType.None),
                BodyPartLinker.GetType(Type, BodyPartDescription.Description)
            );
        }
    }
}
