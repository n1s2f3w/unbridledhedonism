﻿enum KeySetType
{
    None,
    Camera,
    ControlledChar
}

static class Config
{
    internal static bool HideTurnedOnMessages;

    internal static bool DebugViewGoals = false;
    internal static bool DebugViewPreciseValues = false;
    internal static bool DebugViewAllEvents = false;
    internal static bool HidePopupTooltip = false;
    internal static bool AutoCenterCamera = false;
    internal static bool UseMetric = false;
    internal static bool UseInitials = false;
    internal static bool ColoredNames = false;
    internal static bool ColoredPreyNames = false;
    internal static bool AltBellyImage = false;
    internal static bool UseBackgrounds = false;
    internal static bool CropPortraits = false;
    internal static bool DisableDefaultImages = false;
    internal static bool ActionsFirstInSidebar = false;
    internal static bool DisplayStatbars = false;
    internal static bool DetailedWGStats = false;
    internal static bool DetailedVoreStats = false;
    internal static bool HideIncompatibleActions = false;
    internal static bool DisableSetupIcons = false;

    /// <summary>
    /// Use PlayerVisionActive for checks, that checks for observer
    /// </summary>
    internal static bool PlayerVision = false;
    internal static KeySetType Wasd = KeySetType.Camera;
    internal static KeySetType ArrowKeys = KeySetType.Camera;
    internal static KeySetType Numpad = KeySetType.ControlledChar;
    internal static bool HighlightPlayerText = false;
    internal static bool HighlightVore = false;
    internal static bool HighlightSex = false;
    internal static bool HighlightDisposal = false;

    internal static float SuppressVoreMessages = 0;
    internal static float SuppressEndoMessages = 0.75f;

    internal static bool PlayerVisionActive()
    {
        if (State.World.ControlledPerson == null || State.World.PlayerIsObserver())
            return false;
        return PlayerVision;
    }
}
