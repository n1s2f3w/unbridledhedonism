﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Scripts.UI;
using System.IO;

public class GameManager : MonoBehaviour
{
#if UNITY_EDITOR == false
    bool confirmedQuit = false;
#endif

    public GameObject DLCPred;
    public GameObject LastDLCPred; //FallbackAltea
    public GameObject DLCModel; //TargetRawImage

    public GameObject[] playerRooms;

    public GameObject[] targetRooms;

    public GameObject DLCPlayerModel; //PlayerRawImage
    public GameObject DLCPlayer;
    public GameObject LastDLCPlayer;

    bool needCache = false;
    int startFrame;

    bool DLCOn = true;

    public Camera Camera;
    CameraController CameraController;

    public Button[] TurnButtons;

    public BorderDisplay BorderDisplay;

    public GameObject MessageBoxPrefab;

    public TileManager TileManager;

    public TextMeshProUGUI PlayerText;
    public TextMeshProUGUI TargetText;

    public Image PlayerImage;
    public Image TargetImage;

    public Image PlayerBackground;
    public Image TargetBackground;

    public GameModifer GameModifier;

    public VariableEditor VariableEditor;

    public MapEditor MapEditor;

    public MoveController MoveController;

    public StartScreen StartScreen;
    public TitleScreen TitleScreen;
    public MenuScreen MenuScreen;
    public SaveLoadScreen SaveLoadScreen;
    public RelationshipScreen RelationshipScreen;
    public OptionsScreen OptionsScreen;
    public DigestionLogScreen DigestionLogScreen;
    public TraitEditorScreen TraitEditorScreen;
    public TemplateEditorScreen TemplateEditorScreen;

    public MidGameSavedCharacterScreen MidGameSavedCharacterScreen;

    public VoreConfiguration VoreConfiguration;

    public TextMeshProUGUI LogText;

    SidePanelTextPreparer GetText;

    public GameObject DialogBoxPrefab;
    public GameObject ThreeDialogBoxPrefab;

    public HoveringTooltip HoveringTooltip;

    public GameObject TMPText;
    public Transform TextFolder;
    internal Dictionary<Vec2, TextMeshProUGUI> TextDict;

    public TextMeshProUGUI HoveredText;
    public GameObject HoveredPanel;

    internal PortraitController PortraitController;
    internal BackgroundController BackgroundController;

    private Person _clickedPerson;

    public GameObject ContinueActionPanel;
    public TextMeshProUGUI ContinueActionText;
    public Button ContinueActionButton;

    public TMP_InputField SkippedTurnsCount;

    internal PanelType PanelInfoType;

    public GameObject LoadPicker;

    public RaceWeightsScreen RaceWeightsScreen;

    public GenderEditingScreen GenderEditingScreen;

    public GameObject InputBoxPrefab;

    public GameObject RaceInputBoxPrefab;

    internal bool ActiveInput;

    internal float PlayerIdealPortraitWidth;
    internal float TargetIdealPortraitWidth;

    internal int TurnsSkipped;

    RectTransform PlayerRect;
    RectTransform TargetRect;

    RectTransform PlayerBackgroundRect;
    RectTransform TargetBackgroundRect;

    internal RepeatAction RepeatAction;

    public void CenterCameraOnTile(Vec2 Position)
    {
        var loc = new Vector2(Position.x, Position.y);
        loc.y -= Camera.orthographicSize / 2;

        Camera.transform.SetPositionAndRotation(loc, new Quaternion());
    }

    internal void RefreshCameraSettings()
    {
        if (CameraController == null && Camera != null)
            CameraController = Camera.GetComponent<CameraController>();
        if (CameraController == null) //Protection for Tests
            return;
        if (State.World == null)
        {
            Debug.LogWarning("Null World!");
            return;
        }
        CameraController.maxX = State.World.Zones.GetLength(0);
        CameraController.maxY = State.World.Zones.GetLength(1);
        CameraController.ZoomRange.y = Math.Max(CameraController.maxX, CameraController.maxY);
    }

    public bool PlayerVisionCheck(Vec2 tile)
    {
        if (Config.PlayerVisionActive() == false)
            return true;
        else if (LOS.Check(State.World.ControlledPerson, tile))
            return true;
        else if (State.World.ControlledPerson.Position.GetNumberOfMovesDistance(tile) == 1 && State.World.Zones[tile.x, tile.y].Objects.Contains(ObjectType.Bed))
            return true;
        else
            return false;
    }

    public void ViewFirstPage()
    {
        PanelInfoType = PanelType.MainPage;
        DisplayInfo();
    }

    public void ViewSecondPage()
    {
        PanelInfoType = PanelType.InfoPage;
        DisplayInfo();
    }

    public void ViewVorePage()
    {
        PanelInfoType = PanelType.VorePage;
        DisplayInfo();
    }

    public void ViewTargetCastPage()
    {
        PanelInfoType = PanelType.CastTargetPage;
        DisplayInfo();
    }

    public void ViewMiscPage()
    {
        PanelInfoType = PanelType.MiscPage;
        DisplayInfo();
    }

    public enum DisplayedTextType
    {
        Both,
        SelfOnly,
        ObservedOnly
    }

    internal DisplayedTextType MainTextType;

    public void SetDisplayedTextType(int text)
    {
        MainTextType = (DisplayedTextType)text;
        LogText.text = State.World.ControlledPerson?.ListEvents(MainTextType);
    }

    internal Person ClickedPerson
    {
        get => _clickedPerson;
        set
        {
            _clickedPerson = value;
            PanelInfoType = PanelType.MainPage;
            DisplayInfo();
        }
    }

    /// <summary>
    /// must be manually initialized
    /// </summary>
    public DialogBox CreateDialogBox()
    {
        return Instantiate(DialogBoxPrefab).GetComponent<DialogBox>();
    }

    /// <summary>
    /// must be manually initialized
    /// </summary>
    public ThreeDialogBox CreateThreeDialogBox()
    {
        return Instantiate(ThreeDialogBoxPrefab).GetComponent<ThreeDialogBox>();
    }

    public void CreateRaceChanger(Person person)
    {
        var obj = Instantiate(RaceInputBoxPrefab).GetComponent<RaceInputBox>();
        obj.SetData(person);
    }

    private void Start()
    {
        State.GameManager = this;
        TextDict = new Dictionary<Vec2, TextMeshProUGUI>();
        TileManager.DrawWorld();
        Application.wantsToQuit += () => WantsToQuit();
        OptionsScreen.LoadFromStored();
        GetText = new SidePanelTextPreparer();
        PortraitController = new PortraitController();
        BackgroundController = new BackgroundController();
        LoadTextSettings();
        PlayerRect = PlayerImage.GetComponent<RectTransform>();
        TargetRect = TargetImage.GetComponent<RectTransform>();
        PlayerBackgroundRect = PlayerBackground.GetComponent<RectTransform>();
        TargetBackgroundRect = TargetBackground.GetComponent<RectTransform>();
        foreach (var entry in State.InitializeErrors)
        {
            CreateMessageBox(entry);
        }
        State.InitializeErrors.Clear();

        // 3D MOD NEEDS CHAR FILES IN StreamingAssets/Races/Named/3dMod\
        // else, 3D MOD disabled

        string path = "Assets/StreamingAssets/Races/Named/3dMod";
        if (Directory.Exists(path))
            UnityEngine.SceneManagement.SceneManager.LoadScene(
                1,
                UnityEngine.SceneManagement.LoadSceneMode.Additive
            );
        else
            DLCOn = false;

        startFrame = Time.frameCount;
    }

    void LoadTextSettings()
    {
        PlayerText.fontSize = PlayerPrefs.GetFloat("LeftTextSize", 22);
        var leftSize = PlayerPrefs.GetFloat("LeftWindowSize", 400);
        PlayerText.transform.parent.parent.parent.GetComponent<RectTransform>().sizeDelta =
            new Vector2(leftSize, 0);
        PlayerIdealPortraitWidth = PlayerPrefs.GetFloat("LeftPortraitSize", 200);
        TargetText.fontSize = PlayerPrefs.GetFloat("RightTextSize", 22);
        var rightSize = PlayerPrefs.GetFloat("RightWindowSize", 400);
        TargetText.transform.parent.parent.parent.GetComponent<RectTransform>().sizeDelta =
            new Vector2(rightSize, 0);
        TargetIdealPortraitWidth = PlayerPrefs.GetFloat("RightPortraitSize", 200);
        LogText.fontSize = PlayerPrefs.GetFloat("BottomTextSize", 24);
        LogText.transform.parent.parent.parent
            .GetComponent<RectTransform>()
            .rect.Set(leftSize, rightSize, 1920 - leftSize - rightSize, 400);
        LogText.transform.parent.parent.parent.GetComponent<RectTransform>().offsetMin =
            new Vector2(leftSize, 0);
        LogText.transform.parent.parent.parent.GetComponent<RectTransform>().offsetMax =
            new Vector2(-rightSize, 400);
        var scalingfactor = HoveredPanel.transform.parent.localScale.x;
        HoveredPanel.transform.position = new Vector3(
            leftSize * scalingfactor,
            HoveredPanel.transform.position.y,
            0
        );
        HoveredPanel.transform.parent.GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
        ContinueActionPanel
            .GetComponent<RectTransform>()
            .rect.Set(leftSize, rightSize, 1920 - leftSize - rightSize, 400);
        ContinueActionPanel.GetComponent<RectTransform>().offsetMin = new Vector2(leftSize, 400);
        ContinueActionPanel.GetComponent<RectTransform>().offsetMax = new Vector2(-rightSize, 440);
    }

    void Quit()
    {
#if UNITY_EDITOR == false
        confirmedQuit = true;
#endif
        State.TextLogger.WriteOut();
        Application.Quit();
    }

    bool WantsToQuit()
    {
#if UNITY_EDITOR
        {
            DebugManager.WriteLog();
            return true;
        }
#else
        var box = Instantiate(DialogBoxPrefab).GetComponent<DialogBox>();
        box.SetData(() => Quit(), "Quit Game", "Cancel", "Are you sure you want to quit?");
        return confirmedQuit;
#endif
    }

    public void CreateMessageBox(string text)
    {
        MessageBox box = Instantiate(MessageBoxPrefab).GetComponent<MessageBox>();
        box.InfoText.text = text;
    }

    public void CreateMessageBox(string text, Action buttonAction)
    {
        MessageBox box = Instantiate(MessageBoxPrefab).GetComponent<MessageBox>();
        box.InfoText.text = text;
        box.Button.onClick.AddListener(new UnityEngine.Events.UnityAction(buttonAction));
    }

    void UpdateState()
    {
        //might put Has3dMod(); here?

        MoveController.UpdateButtons();

        TileManager.UpdateSpecial();

        Dictionary<Vec2, string> personTypes = new Dictionary<Vec2, string>();
        var people = State.World.GetPeople(false);
        foreach (Person person in people)
        {
            if (PlayerVisionCheck(person.Position) == false)
                continue;
            string character;
            if (Config.UseInitials)
            {
                if (person == State.World.ControlledPerson)
                {
                    if (person.VoreController.HasAnyPrey())
                        character = $"<color=green><b>{person.Initials}+</b></color> ";
                    else
                        character = $"<color=green><b>{person.Initials}</b></color> ";
                }
                else
                {
                    if (Config.ColoredNames)
                    {
                        if (person.VoreController.HasAnyPrey())
                            character =
                                $"<color=#{person.GenderType.Color}>{person.Initials}+</color> ";
                        else
                            character =
                                $"<color=#{person.GenderType.Color}>{person.Initials}</color> ";
                    }
                    else
                    {
                        if (person.VoreController.HasAnyPrey())
                            character = $"{person.Initials}+ ";
                        else
                            character = $"{person.Initials} ";
                    }
                }
            }
            else
            {
                if (person == State.World.ControlledPerson)
                {
                    if (person.VoreController.HasAnyPrey())
                        character = $"<color=green><b>{person.Label}+</b></color> ";
                    else
                        character = $"<color=green><b>{person.Label}</b></color> ";
                }
                else
                {
                    if (Config.ColoredNames)
                    {
                        if (person.VoreController.HasAnyPrey())
                            character =
                                $"<color=#{person.GenderType.Color}>{person.Label}+</color> ";
                        else
                            character =
                                $"<color=#{person.GenderType.Color}>{person.Label}</color> ";
                    }
                    else
                    {
                        if (person.VoreController.HasAnyPrey())
                            character = $"{person.Label}+ ";
                        else
                            character = $"{person.Label} ";
                    }
                }
            }

            if (personTypes.ContainsKey(person.Position))
                personTypes[person.Position] += character;
            else
                personTypes[person.Position] = character;
        }

        //#warning LOS Debug
        //        for (int i = 0; i < State.World.Zones.GetLength(0); i++)
        //        {
        //            for (int j = 0; j < State.World.Zones.GetLength(1); j++)
        //            {
        //                if (State.World.GetZone(new Vec2(i,j)) != null && State.World.ControlledPerson.Position != new Vec2(i,j))
        //                {
        //                    personTypes[new Vec2(i, j)] = LOS.Check(State.World.ControlledPerson.Position, new Vec2(i, j)) ? "X" : "";
        //                }
        //            }
        //        }


        foreach (var entry in TextDict)
        {
            entry.Value.text = "";
        }

        foreach (var entry in personTypes)
        {
            if (TextDict.ContainsKey(entry.Key))
            {
                TextDict[entry.Key].text = entry.Value;
            }
            else
            {
                var obj = Instantiate(TMPText, TextFolder).GetComponent<TextMeshProUGUI>();
                obj.transform.SetPositionAndRotation(
                    new Vector3(entry.Key.x, entry.Key.y, 0),
                    new Quaternion()
                );
                TextDict[entry.Key] = obj;
                obj.text = entry.Value.ToString();
            }
        }

        LogText.text = State.World.ControlledPerson.ListEvents(MainTextType);
        if (State.World.PlayerIsObserver() && State.World.VisionAttachedTo != null)
        {
            LogText.text = State.World.VisionAttachedTo.ListEvents(MainTextType);
        }
    }

    private void Update()
    {
        if (ActiveInput)
            return;

        Vector2 currentMousePos = State.GameManager.Camera.ScreenToWorldPoint(Input.mousePosition);

        int x = (int)(currentMousePos.x + 0.5f);
        int y = (int)(currentMousePos.y + 0.5f);
        if (State.GameManager.MapEditor.gameObject.activeSelf)
        {
            DisplayHoveredPanel(x, y);
            UpdateState();
            return;
        }

        if (
            Input.GetKey(KeyCode.F)
            && (
                Input.GetKey(KeyCode.LeftControl)
                || Input.GetKey(KeyCode.RightControl)
                || Input.GetKey(KeyCode.LeftCommand)
                || Input.GetKey(KeyCode.RightCommand)
            )
        )
            GetText.ListAllPeople();

        if (State.World.RemainingSkippedTurns > 0)
        {
            TurnsSkipped = 0;
            State.World.NextTurn(true, true);
        }
        if (
            State.World.ControlledPerson == null
            && State.World.RemainingSkippedTurns == 0
            && State.World.GetPeople(true).Any()
        )
            State.World.ControlledPerson = State.World.GetPeople(true)[0];

        if (
            Input.GetButtonDown("Menu")
            && StartScreen.gameObject.activeSelf == false
            && TitleScreen.gameObject.activeSelf == false
        )
        {
            MenuScreen.gameObject.SetActive(!MenuScreen.gameObject.activeSelf);
        }

        if (
            State.World.CurrentTurn == State.World.ControlledPerson
            && State.World.ClearedNextTurn == false
            && MenuScreen.gameObject.activeSelf == false
            && TitleScreen.gameObject.activeSelf == false
            && StartScreen.gameObject.activeSelf == false
        )
        {
            if (State.KeyManager.SubmitPressed)
            {
                if (
                    FindObjectOfType<MessageBox>() == null
                    && FindObjectOfType<DialogBox>() == null
                    && FindObjectOfType<ThreeDialogBox>() == null
                    && State.GameManager.ContinueActionButton.interactable
                    && State.GameManager.ContinueActionPanel.activeSelf
                )
                {
                    State.GameManager.ContinueActionButton.onClick.Invoke();
                    return;
                }
            }
            else if (State.KeyManager.AIControlPressed)
            {
                if (
                    FindObjectOfType<MessageBox>() == null
                    && FindObjectOfType<DialogBox>() == null
                    && FindObjectOfType<ThreeDialogBox>() == null
                )
                {
                    SkipOneTurn();
                    return;
                }
            }
            else if (State.KeyManager.WaitPressed)
            {
                if (
                    FindObjectOfType<MessageBox>() == null
                    && FindObjectOfType<DialogBox>() == null
                    && FindObjectOfType<ThreeDialogBox>() == null
                )
                {
                    MoveController.SkipTurn();
                    return;
                }
            }
            else if (State.KeyManager.CenterCameraOnPlayerPressed)
            {
                if (
                    FindObjectOfType<MessageBox>() == null
                    && FindObjectOfType<DialogBox>() == null
                    && FindObjectOfType<ThreeDialogBox>() == null
                )
                {
                    State.GameManager.CenterCameraOnTile(State.World.ControlledPerson.Position); //existance is confirmed above
                    return;
                }
            }
            if (State.KeyManager.RepeatActionPressed)
            {
                if (
                    FindObjectOfType<MessageBox>() == null
                    && FindObjectOfType<DialogBox>() == null
                    && FindObjectOfType<ThreeDialogBox>() == null
                )
                {
                    State.GameManager.RepeatAction.RepeatLast();
                    return;
                }
            }
            else if (
                (Config.Wasd == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.D))
                || (
                    Config.ArrowKeys == KeySetType.ControlledChar
                    && Input.GetKeyDown(KeyCode.RightArrow)
                )
                || (Config.Numpad == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.Keypad6))
            )
            {
                if (
                    MoveController.EastButton.interactable
                    && MoveController.EastButton.gameObject.activeSelf
                )
                {
                    MoveController.MoveEast();
                    return;
                }
            }
            else if (
                (Config.Wasd == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.A))
                || (
                    Config.ArrowKeys == KeySetType.ControlledChar
                    && Input.GetKeyDown(KeyCode.LeftArrow)
                )
                || (Config.Numpad == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.Keypad4))
            )
            {
                if (
                    MoveController.WestButton.interactable
                    && MoveController.WestButton.gameObject.activeSelf
                )
                {
                    MoveController.MoveWest();
                    return;
                }
            }
            else if (
                (Config.Wasd == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.W))
                || (
                    Config.ArrowKeys == KeySetType.ControlledChar
                    && Input.GetKeyDown(KeyCode.UpArrow)
                )
                || (Config.Numpad == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.Keypad8))
            )
            {
                if (
                    MoveController.NorthButton.interactable
                    && MoveController.NorthButton.gameObject.activeSelf
                )
                {
                    MoveController.MoveNorth();
                    return;
                }
            }
            else if (
                (Config.Wasd == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.S))
                || (
                    Config.ArrowKeys == KeySetType.ControlledChar
                    && Input.GetKeyDown(KeyCode.DownArrow)
                )
                || (Config.Numpad == KeySetType.ControlledChar && Input.GetKeyDown(KeyCode.Keypad2))
            )
            {
                if (
                    MoveController.SouthButton.interactable
                    && MoveController.SouthButton.gameObject.activeSelf
                )
                {
                    MoveController.MoveSouth();
                    return;
                }
            }
        }

        UpdateState();

        if (EventSystem.current.IsPointerOverGameObject() == false) //Makes sure mouse isn't over a UI element
        {
            if (Input.GetMouseButtonDown(0))
            {
                ProcessLeftClick(x, y);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                ProcessRightClick(x, y);
            }
        }

        State.World.CheckNextTurn();

        DisplayHoveredPanel(x, y);
    }

    private void LateUpdate()
    {
        if (DLCOn == false)
            return;
        if (needCache == false && Time.frameCount > 3 + startFrame)
        {
            LastDLCPred = GameObject.Find("Altea");
            DLCPred = LastDLCPred;
            LastDLCPlayer = LastDLCPred;
            DLCPlayer = GameObject.Find("AlteaPlayer");

            var temp = GameObject.Find("PlayerCam");
            for (int i = 0; i < 11; i++)
            {
                playerRooms[i] = temp.transform.GetChild(i).gameObject;
            }

            temp = GameObject.Find("TargetCam");
            for (int i = 0; i < 11; i++)
            {
                targetRooms[i] = temp.transform.GetChild(i).gameObject;
            }
            needCache = true;
        }
    }

    private void DisplayHoveredPanel(int x, int y)
    {
        if (State.World.TileExists(x, y))
        {
            HoveredPanel.SetActive(true);

            int count = 0;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(State.World.Zones[x, y].Name);
            if (State.World.Zones[x, y].Objects.Any())
            {
                count++;
                sb.AppendLine("Has: " + string.Join(", ", State.World.Zones[x, y].Objects));
            }

            if (
                Config.PlayerVisionActive() == false
                || PlayerVisionCheck(new Vec2 (x, y))
            )
            {
                foreach (var person in State.World.GetPeople(false))
                {
                    if (person.Position.x == x && person.Position.y == y)
                    {
                        count++;
                        if (Config.UseInitials)
                            sb.AppendLine($"{person.FirstName} ({person.Initials})");
                        else
                            sb.AppendLine($"{person.FirstName} ({person.Label})");
                    }
                }
            }

            var rect = HoveredPanel.GetComponent<RectTransform>();
            rect.sizeDelta = new Vector2(200, 25 * (count + 1));
            HoveredText.text = sb.ToString();
        }
        else
        {
            HoveredPanel.SetActive(false);
        }
    }

    void ProcessLeftClick(int x, int y)
    {
        GetText.InfoForSquare(x, y);
    }

    void ProcessRightClick(int x, int y)
    {
        if (
            FindObjectOfType<DialogBox>() != null
            || State.World.ControlledPerson.BeingEaten
            || State.World.ControlledPerson.ActiveSex != null
        )
            return;
        var path = PathFinder.GetPath(
            State.World.ControlledPerson.Position,
            new Vec2(x, y),
            State.World.ControlledPerson
        );
        if (path == null || path.Count == 0)
            return;
        State.World.Move(
            State.World.ControlledPerson,
            path[0].x - State.World.ControlledPerson.Position.x,
            path[0].y - State.World.ControlledPerson.Position.y
        );
        TurnsSkipped = 0;
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            State.World.AutoDestination = new Vec2(x, y);
        State.World.ControlledPerson.AI.ClearTasks();
        State.World.NextTurn();
    }

    internal void DisplayInfo()
    {
        if (State.World.PlayerIsObserver() && State.World.VisionAttachedTo == null)
            State.World.VisionAttachedTo = State.World.GetPeople(false).FirstOrDefault();
        if (State.World.PlayerIsObserver() && State.World.VisionAttachedTo == null)
            return;
        if (ClickedPerson != null &&
            PlayerVisionCheck(ClickedPerson.Position) == false
        )
            ClickedPerson = null;
        UpdatePortraits();
        GetText.GetInfo();
    }

    internal void Has3dMod(
        GameObject dlcPortrait,
        GameObject[] currentRooms,
        Person currentPerson,
        string isPlayer
    ) //the3dmod stuff
    {
        if (GameObject.Find(currentPerson.FirstName) != null)
        {
            dlcPortrait = GameObject.Find(
                currentPerson.Picture.Substring(0, currentPerson.Picture.Length - 5) + isPlayer
            );
            if (LastDLCPlayer != dlcPortrait && isPlayer == "Player") //handles charcter swaps to hide old game player objects
            {
                LastDLCPlayer.transform.GetChild(0).gameObject.SetActive(false);
                LastDLCPlayer = dlcPortrait;
            }
            if (LastDLCPred != dlcPortrait && isPlayer == "") //handles target swaps to hide old game objects
            {
                LastDLCPred.transform.GetChild(0).gameObject.SetActive(false);
                LastDLCPred = dlcPortrait;
            }
        }
        else //fallback incase above is null
        {
            dlcPortrait = LastDLCPred;
        }

        if (dlcPortrait == null) //Handling code to protect invalid links
            return;

        //makes the first child of ths found game object active (make the top child be the charcter model)
        dlcPortrait.transform.GetChild(0).gameObject.SetActive(true);

        //Disposal & Animators
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool("Fap", currentPerson.StreamingSelfAction == SelfActionType.Masturbate);
        dlcPortrait
            .GetComponent<Animator>()
            .SetFloat("Belly", currentPerson.VoreController.BellySize());
        dlcPortrait
            .GetComponent<Animator>()
            .SetFloat("Balls", currentPerson.VoreController.BallsSize());
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "CVToilet",
                currentPerson.StreamingSelfAction == SelfActionType.CockDisposalBathroom
            );
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "CVFloor",
                currentPerson.StreamingSelfAction == SelfActionType.CockDisposalFloor
            );
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "UBFloor",
                currentPerson.StreamingSelfAction == SelfActionType.UnbirthDisposalFloor
            );
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "DisFloor",
                currentPerson.StreamingSelfAction == SelfActionType.ScatDisposalFloor
            );
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "DisToilet",
                currentPerson.StreamingSelfAction == SelfActionType.ScatDisposalBathroom
                    || currentPerson.StreamingSelfAction == SelfActionType.UnbirthDisposalBathroom
            );
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "UBToilet",
                currentPerson.StreamingSelfAction == SelfActionType.UnbirthDisposalBathroom
            );

        //death states
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool("Eaten", currentPerson.BeingEaten && !currentPerson.Dead);

        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "DeadCV",
                currentPerson.Dead
                    && currentPerson.VoreTracking.LastOrDefault().Location.ToString().ToLower()
                        == "balls"
            );
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "DeadUB",
                currentPerson.Dead
                    && currentPerson.VoreTracking.LastOrDefault().Location.ToString().ToLower()
                        == "womb"
            );
        dlcPortrait
            .GetComponent<Animator>()
            .SetBool(
                "Dead",
                currentPerson.Dead
                    && currentPerson.VoreTracking.LastOrDefault().Location.ToString().ToLower()
                        == "stomach"
                    || currentPerson.Dead
                        && currentPerson.VoreTracking.LastOrDefault().Location.ToString().ToLower()
                            == "bowels"
            );

        //set'Rooms'
        currentRooms[0].SetActive(currentPerson.BeingEaten);
        currentRooms[1].SetActive(currentPerson.ZoneContainsBed() && !currentPerson.BeingEaten);
        currentRooms[2].SetActive(
            currentPerson.ZoneContainsBathroom() && !currentPerson.BeingEaten
        );
        currentRooms[3].SetActive(
            State.World.Zones[currentPerson.Position.x, currentPerson.Position.y].Type
                == ZoneType.IndoorHallway
                && !currentPerson.BeingEaten
        );
        currentRooms[4].SetActive(
            State.World.Zones[currentPerson.Position.x, currentPerson.Position.y].Type
                == ZoneType.OutdoorPath
                && !currentPerson.BeingEaten
        );
        currentRooms[5].SetActive(
            State.World.Zones[currentPerson.Position.x, currentPerson.Position.y].Type
                == ZoneType.Grass
                && !currentPerson.BeingEaten
        );
        currentRooms[6].SetActive(currentPerson.ZoneContainsGym() && !currentPerson.BeingEaten);
        currentRooms[7].SetActive(currentPerson.ZoneContainsShower() && !currentPerson.BeingEaten);
        currentRooms[8].SetActive(
            currentPerson.ZoneContainsNurseOffice() && !currentPerson.BeingEaten
        );
        currentRooms[9].SetActive(currentPerson.ZoneContainsLibrary() && !currentPerson.BeingEaten);
        currentRooms[10].SetActive(currentPerson.ZoneContainsFood() && !currentPerson.BeingEaten);

        //set blendshape animations based on ingame values
        dlcPortrait.GetComponent<Animator>().SetFloat("Horny", currentPerson.Needs.Horniness);
        dlcPortrait.GetComponent<Animator>().SetFloat("Hungry", currentPerson.Needs.Hunger / 2);
        dlcPortrait
            .GetComponent<Animator>()
            .SetFloat("Fat", (float)currentPerson.VoreController.TotalDigestions / 20);

        //set clothing items
        if (currentPerson.CharacterFullyClothed)
            dlcPortrait.GetComponent<Animator>().SetInteger("Dressed", 0);

        if (currentPerson.CharacterInUnderwear)
            dlcPortrait.GetComponent<Animator>().SetInteger("Dressed", 1);

        if (currentPerson.CharacterNude)
            dlcPortrait.GetComponent<Animator>().SetInteger("Dressed", 2);
    }

    internal void UpdatePortraits()
    { //DLCPlayer Player3dMod()
        if (State.World.ControlledPerson.Picture?.ToLower().Contains("3dmod") ?? false)
        { //enter the dlc function
            Has3dMod(DLCPlayer, playerRooms, State.World.ControlledPerson, "Player");
            DLCPlayerModel.SetActive(true);
        }
        else
        {
            DLCPlayerModel.SetActive(false);
        }

        PlayerImage.sprite = PortraitController.GetPicture(State.World.ControlledPerson);
        PlayerBackground.sprite = BackgroundController.GetBackground(State.World.ControlledPerson);

        if (PlayerImage.sprite == null && !DLCPlayerModel.activeSelf)
        {
            PlayerText.margin = new Vector4(0, 0, 0, 0);
            PlayerImage.gameObject.SetActive(false);
            PlayerBackground.gameObject.SetActive(false);
            DLCPlayerModel.SetActive(false);
        }
        else
        {
            PlayerImage.gameObject.SetActive(true);

            if (BackgroundController.GetBackground(State.World.ControlledPerson) && Config.UseBackgrounds)
                PlayerBackground.gameObject.SetActive(true);
            else
                PlayerBackground.gameObject.SetActive(false);

            if (DLCPlayerModel.activeSelf) //makes the margin fit the players 3d model
            {
                PlayerText.margin = new Vector4(0, 820, 0, 0);
                PlayerImage.gameObject.SetActive(false);
                PlayerBackground.gameObject.SetActive(false);
            }
            else
            {
                int imgWidth = (int)PlayerImage.sprite.rect.width;

                var texRatio = ((float)PlayerImage.sprite.rect.height / (float)PlayerImage.sprite.rect.width);
                var targetRatio = 1.6;

                if (Config.CropPortraits && texRatio < targetRatio)
                {
                    imgWidth = (int)(PlayerImage.sprite.rect.height / targetRatio);
                }

                float adjust = PlayerIdealPortraitWidth / imgWidth;
                
                PlayerRect.sizeDelta = new Vector2(
                    PlayerImage.sprite.rect.width * adjust,
                    PlayerImage.sprite.rect.height * adjust
                );
                
                PlayerBackgroundRect.sizeDelta = new Vector2(
                    PlayerBackground.sprite.rect.width / (PlayerBackground.sprite.rect.height / (PlayerImage.sprite.rect.height * adjust)),
                    PlayerImage.sprite.rect.height * adjust
                );
                
                PlayerText.margin = new Vector4(0, PlayerRect.sizeDelta.y, 0, 0);
            }
        }

        //might be able to intercept here for 3d Altea
        if (ClickedPerson != null && ClickedPerson != State.World.ControlledPerson)
        {
            if (ClickedPerson.Picture?.ToLower().Contains("3dmod") ?? false)
            { //enter dlc function for target
                Has3dMod(DLCPred, targetRooms, ClickedPerson, "");
                DLCModel.SetActive(true);
            }
            else //leave this for old pics to still load
            {
                TargetImage.sprite = PortraitController.GetPicture(ClickedPerson);
                TargetBackground.sprite = BackgroundController.GetBackground(ClickedPerson);
                DLCModel.SetActive(false);
            }
        }
        else
        {
            TargetImage.sprite = null;
            DLCModel.SetActive(false);
        }

        if (TargetImage.sprite == null && !DLCModel.activeSelf)
        {
            TargetText.margin = new Vector4(0, 0, 0, 0);
            TargetImage.gameObject.SetActive(false);
            TargetBackground.gameObject.SetActive(false);
            DLCModel.SetActive(false);
        }
        else
        {
            TargetImage.gameObject.SetActive(true);

            if (BackgroundController.GetBackground(ClickedPerson) && Config.UseBackgrounds)
                TargetBackground.gameObject.SetActive(true);
            else
                TargetBackground.gameObject.SetActive(false);

            if (DLCModel.activeSelf) //makes the margin fit the targets 3d model
            {
                TargetText.margin = new Vector4(0, 820, 0, 0);
                TargetImage.gameObject.SetActive(false);
                TargetBackground.gameObject.SetActive(false);
            }
            else
            {
                int imgWidth = (int)TargetImage.sprite.rect.width;

                var texRatio = ((float)TargetImage.sprite.rect.height / (float)TargetImage.sprite.rect.width);
                var targetRatio = 1.6;

                if (Config.CropPortraits && texRatio < targetRatio)
                {
                    imgWidth = (int)(TargetImage.sprite.rect.height / targetRatio);
                }

                float adjust = TargetIdealPortraitWidth / imgWidth;

                TargetRect.sizeDelta = new Vector2(
                    TargetImage.sprite.rect.width * adjust,
                    TargetImage.sprite.rect.height * adjust
                );

                TargetBackgroundRect.sizeDelta = new Vector2(
                    TargetBackground.sprite.rect.width / (TargetBackground.sprite.rect.height / (TargetImage.sprite.rect.height * adjust)),
                    TargetImage.sprite.rect.height * adjust
                );

                TargetText.margin = new Vector4(0, TargetRect.sizeDelta.y, 0, 0);
            }
        }
    }

    public void SkipOneTurn()
    {
        State.World.NextTurn(true, true);
    }

    public void SkipManyTurns()
    {
        if (int.TryParse(SkippedTurnsCount.text, out int result))
            State.World.RemainingSkippedTurns = result;
    }
}
