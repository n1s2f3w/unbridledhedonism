﻿using System;

public struct Vec2 : IComparable<Vec2>, IEquatable<Vec2>
{
    public int x,
        y;

    public Vec2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static Vec2 operator +(Vec2 a, Vec2 b)
    {
        return new Vec2(a.x + b.x, a.y + b.y);
    }

    public static bool operator ==(Vec2 a, Vec2 b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Vec2 a, Vec2 b)
    {
        return !(a == b);
    }

    public override string ToString()
    {
        return "(" + x + ", " + y + ")";
    }

    public int CompareTo(Vec2 other)
    {
        if (x == other.x)
        {
            return y.CompareTo(other.y);
        }
        else
        {
            return x.CompareTo(other.x);
        }
    }

    public bool Equals(Vec2 other)
    {
        return other == this;
    }

    public override int GetHashCode()
    {
        return ((x.GetHashCode() ^ (y.GetHashCode() << 1)) >> 1);
    }

    public override bool Equals(object obj)
    {
        if (obj is Vec2)
        {
            return (Vec2)obj == this;
        }

        return false;
    }

    public float GetDistance(Vec2 p)
    {
        float xe = Math.Abs(x - p.x);
        float ye = Math.Abs(y - p.y);
        xe = xe * xe;
        ye = ye * ye;

        return (float)Math.Sqrt(xe + ye);
    }

    //public int GetNumberOfMovesDistance(Vec2 p) => Math.Max(Math.Abs(p.x - x), Math.Abs(p.y - y)); Diagonal version
    public int GetNumberOfMovesDistance(Vec2 p) => Math.Abs(p.x - x) + Math.Abs(p.y - y);
}
