﻿using System;
using System.IO;
using UnityEngine;

public class ExceptionHandler : MonoBehaviour
{
    int exceptionCount;
    string path;

    void Awake()
    {
        if (Application.isEditor)
            return;

        Application.logMessageReceived += HandleException;
        if (Application.platform == RuntimePlatform.OSXPlayer)
            path = Path.Combine(Application.persistentDataPath, "recentexceptions.txt");
        else
            path = Path.Combine(Application.dataPath, "recentexceptions.txt");
    }

    private void HandleException(string condition, string stackTrace, LogType type)
    {
        try
        {
            using (StreamWriter writer = new StreamWriter(path))
            {
                if (type == LogType.Exception)
                {
                    if (exceptionCount > 50) //To avoid too much clutter
                        return;
                    if (exceptionCount == 0)
                    {
                        State.GameManager.CreateMessageBox(
                            "An exception just occured, unusual behavior may result.  Only the first one in a session gives a warning."
                        );
                    }

                    writer.WriteLine(
                        $"{type}: {condition}\nVersion :{State.Version}\n{stackTrace}"
                    );
                    writer.Flush();
                    exceptionCount++;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            Debug.Log(
                "Failed to write exception... just eating it rather than throw another exception"
            );
        }
    }
}
