﻿using System;
using UnityEngine;

class KeyManager
{
    public KeyManager()
    {
        Submit = GetKeyFromString(PlayerPrefs.GetString("Submit", "Return"));
        Submit2 = GetKeyFromString(PlayerPrefs.GetString("Submit2", "KeypadEnter"));
        Cancel = GetKeyFromString(PlayerPrefs.GetString("Cancel", "Space"));
        AIControl = GetKeyFromString(PlayerPrefs.GetString("AIControl", "Slash"));
        Wait = GetKeyFromString(PlayerPrefs.GetString("Wait", "Keypad5"));
        CenterCameraOnPlayer = GetKeyFromString(
            PlayerPrefs.GetString("CenterCameraOnPlayer", "Period")
        );
        RepeatAction = GetKeyFromString(PlayerPrefs.GetString("RepeatAction", "R"));
    }

    internal bool SubmitPressed => Input.GetKeyDown(Submit) || Input.GetKeyDown(Submit2);
    internal bool CancelPressed => Input.GetKeyDown(Cancel);
    internal bool AIControlPressed => Input.GetKeyDown(AIControl);
    internal bool WaitPressed => Input.GetKeyDown(Wait);
    internal bool CenterCameraOnPlayerPressed => Input.GetKeyDown(CenterCameraOnPlayer);
    internal bool RepeatActionPressed => Input.GetKeyDown(RepeatAction);

    internal string SubmitKey => Submit.ToString();
    internal string Submit2Key => Submit2.ToString();
    internal string CancelKey => Cancel.ToString();
    internal string AIControlKey => AIControl.ToString();
    internal string WaitKey => Wait.ToString();
    internal string CenterCameraOnPlayerKey => CenterCameraOnPlayer.ToString();
    internal string RepeatActionKey => RepeatAction.ToString();

    internal void ChangeSubmitKey(KeyCode code)
    {
        Submit = code;
        PlayerPrefs.SetString("Submit", code.ToString());
    }

    internal void ChangeSubmit2Key(KeyCode code)
    {
        Submit2 = code;
        PlayerPrefs.SetString("Submit2", code.ToString());
    }

    internal void ChangeCancelKey(KeyCode code)
    {
        Cancel = code;
        PlayerPrefs.SetString("Cancel", code.ToString());
    }

    internal void ChangeAIControlKey(KeyCode code)
    {
        AIControl = code;
        PlayerPrefs.SetString("AIControl", code.ToString());
    }

    internal void ChangeWaitKey(KeyCode code)
    {
        Wait = code;
        PlayerPrefs.SetString("Wait", code.ToString());
    }

    internal void ChangeCenterCameraOnPlayerKey(KeyCode code)
    {
        CenterCameraOnPlayer = code;
        PlayerPrefs.SetString("CenterCameraOnPlayer", code.ToString());
    }

    internal void ChangeRepeatActionKey(KeyCode code)
    {
        RepeatAction = code;
        PlayerPrefs.SetString("RepeatAction", code.ToString());
    }

    KeyCode GetKeyFromString(string str)
    {
        if (Enum.TryParse(str, out KeyCode code))
        {
            return code;
        }
        return KeyCode.None;
    }

    KeyCode Submit;
    KeyCode Submit2;
    KeyCode Cancel;
    KeyCode AIControl;
    KeyCode Wait;
    KeyCode CenterCameraOnPlayer;
    KeyCode RepeatAction;
}
