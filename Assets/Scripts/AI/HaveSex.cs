﻿using OdinSerializer;

namespace Assets.Scripts.AI
{
    class HaveSex : IGoal
    {
        [OdinSerialize]
        Person Self;

        [OdinSerialize]
        bool Pred = false;

        [OdinSerialize]
        bool PredAsk = false;

        public HaveSex(Person self)
        {
            float mult = 1;
            if (self.HasTrait(Traits.VoraciousLover))
                mult = 2.5f;
            else if (self.HasTrait(Traits.LoversEmbrace))
                mult = 0;
            Self = self;
            if (
                self.VoreController.CapableOfVore() == false
                || self.VoreController.TargetVoreImmune(
                    self.ActiveSex.Other,
                    DigestionAlias.CanVore
                )
                || (
                    State.World.Settings.SecretiveVore
                    && HelperFunctions.InPrivateArea(self, self) == false
                )
            )
                Pred = false;
            else
            {
                if (
                    Self.AI.Desires.DesireToForciblyEatAndDigestTarget(self.ActiveSex.Other) * mult
                    > Rand.NextFloat(0, 10)
                )
                {
                    Pred = true;
                    PredAsk = false;
                }
                else if (
                    Self.AI.Desires.DesireToVoreAndDigestTarget(self.ActiveSex.Other) * mult
                    > Rand.NextFloat(0, 10)
                )
                {
                    Pred = true;
                    PredAsk = true;
                }
            }
        }

        public GoalReturn ExecuteStep()
        {
            if (Self.ActiveSex != null)
            {
                if (
                    Self.GetRelationshipWith(Self.ActiveSex.Other).FriendshipLevel < -.3f
                    || Self.Position != Self.ActiveSex.Other.Position
                    || Self.ActiveSex.Other.ActiveSex?.Other != Self
                )
                {
                    Self.EndStreamingActions();
                    return GoalReturn.CompletedGoal;
                }
                
                // Strip self and partner at certain turn intervals
                if (Self.ActiveSex.Turns > 4 && Self.ClothingStatus == ClothingStatus.Normal)
                {
                    SexInteractionList.List[SexInteractionType.StripSelf].OnDo(
                        Self,
                        Self.ActiveSex.Other
                    );
                    return GoalReturn.DidStep;
                }
                if (
                    Self.ActiveSex.Turns > 7
                    && Self.ActiveSex.Other.ClothingStatus == ClothingStatus.Normal
                )
                {
                    SexInteractionList.List[SexInteractionType.StripThem].OnDo(
                        Self,
                        Self.ActiveSex.Other
                    );
                    return GoalReturn.DidStep;
                }
                if (Self.ActiveSex.Turns > 9 && Self.ClothingStatus == ClothingStatus.Underwear)
                {
                    SexInteractionList.List[SexInteractionType.StripSelf].OnDo(
                        Self,
                        Self.ActiveSex.Other
                    );
                    return GoalReturn.DidStep;
                }
                if (
                    Self.ActiveSex.Turns > 12
                    && Self.ActiveSex.Other.ClothingStatus == ClothingStatus.Underwear
                )
                {
                    SexInteractionList.List[SexInteractionType.StripThem].OnDo(
                        Self,
                        Self.ActiveSex.Other
                    );
                    return GoalReturn.DidStep;
                }

                // Stop having sex after about 50 turns, longer if has insatiable
                if (Rand.Next(30) + Self.ActiveSex.Turns > 50 + (Self.HasTrait(Quirks.Insatiable) ? 20 : 0))
                {
                    SexInteractionList.List[SexInteractionType.EndSex].OnDo(
                        Self,
                        Self.ActiveSex.Other
                    );
                    return GoalReturn.CompletedGoal;
                }
                else
                {
                    // Random chance for preds to initiate vore during sex
                    SexInteractionBase pick = null;
                    if (Pred && Rand.Next(8) == 0)
                    {
                        SexInteractionType type;
                        if (PredAsk)
                        {
                            type = InteractionPicker.SexAskEatDigest(Self, Self.ActiveSex.Other);
                        }
                        else
                        {
                            type = InteractionPicker.SexForceEat(Self, Self.ActiveSex.Other);
                        }

                        if (type != SexInteractionType.StripSelf)
                            pick = SexInteractionList.List[type];
                        else
                            pick = SexInteractionList.GetRandomAvailable(
                                Self,
                                Self.ActiveSex.Other
                            );
                    }
                    else if (
                        Self.ActiveSex.Turns > 14
                        && Self.HasTrait(Traits.LoversEmbrace) == false
                        && Self.AI.Desires.DesireToEndoTarget(Self.ActiveSex.Other)
                            > Rand.NextFloat(0, 12 / (Self.HasTrait(Traits.VoraciousLover) ? 2 : 1))
                    )
                    {
                        var type = InteractionPicker.SexAskEatEndo(Self, Self.ActiveSex.Other);

                        if (type != SexInteractionType.StripSelf)
                            pick = SexInteractionList.List[type];
                        else
                            pick = SexInteractionList.GetRandomAvailable(
                                Self,
                                Self.ActiveSex.Other
                            );
                    }
                    
                    else if (Self.ActiveSex.LastPositionChange > 4 && Rand.Next(5) == 0)
                        pick = SexInteractionList.GetRandomPosition(Self, Self.ActiveSex.Other);
                    else
                        pick = SexInteractionList.GetRandomAvailable(Self, Self.ActiveSex.Other);

                    pick.OnDo(Self, Self.ActiveSex.Other);
                    return GoalReturn.DidStep;
                }
            }
            else
            {
                return GoalReturn.AbortGoal;
            }
        }

        public string ReportGoal()
        {
            return "Having Sex";
        }
    }
}
