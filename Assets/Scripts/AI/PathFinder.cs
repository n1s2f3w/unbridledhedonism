﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class PathFinder
{
    public static bool Initialized = false;

    public static Person Person;

    struct Cell
    {
        public bool Exists;
        public bool Locked;
    }

    struct QueueNode : IComparable<QueueNode>
    {
        public Vec2 Value;
        public int Dist;
        public int Diagonals;

        public QueueNode(Vec2 value, int dist, int diagonals)
        {
            Value = value;
            Dist = dist;
            Diagonals = diagonals;
        }

        public int CompareTo(QueueNode other)
        {
            if (Dist != other.Dist)
            {
                return Dist.CompareTo(other.Dist);
            }
            else if (Diagonals != other.Diagonals)
            {
                return Diagonals.CompareTo(other.Diagonals);
            }
            else
                return Value.CompareTo(other.Value);
        }
    }

    class Vec2Comparer : IEqualityComparer<Vec2>
    {
        public bool Equals(Vec2 a, Vec2 b)
        {
            return a == b;
        }

        public int GetHashCode(Vec2 obj)
        {
            return ((IntegerHash(obj.x) ^ (IntegerHash(obj.y) << 1)) >> 1);
        }

        static int IntegerHash(int a)
        {
            // fmix32 from murmurhash
            uint h = (uint)a;
            h ^= h >> 16;
            h *= 0x85ebca6bU;
            h ^= h >> 13;
            h *= 0xc2b2ae35U;
            h ^= h >> 16;
            return (int)h;
        }
    }

    struct EndPoint
    {
        internal QueueNode node;
        internal int priority;

        public EndPoint(QueueNode node, int priority)
        {
            this.node = node;
            this.priority = priority;
        }
    }

    private static Cell[,] Grid = null;

    private static List<Vec2> GetNeighbours(Vec2 cell, List<Vec2> neighbours)
    {
        neighbours.Clear();

        for (int dx = -1; dx <= 1; dx++)
        {
            for (int dy = -1; dy <= 1; dy++)
            {
                //Prevent diagonals
                if (Math.Abs(dx + dy) != 1)
                    continue;

                if (
                    State.World.BorderController
                        .GetBorder(cell, new Vec2(cell.x + dx, cell.y + dy))
                        .BlocksTravel
                )
                    continue;

                var coord = cell + new Vec2(dx, dy);

                //bool notSelf = !(dx == 0 && dy == 0); Not needed if prevent diagonals is on
                bool withinGrid =
                    coord.x >= 0
                    && coord.y >= 0
                    && coord.x <= State.World.Zones.GetUpperBound(0)
                    && coord.y <= State.World.Zones.GetUpperBound(1);

                if (withinGrid)
                {
                    neighbours.Add(coord);
                }
            }
        }

        return neighbours;
    }

    private static List<Vec2> FindPath(Vec2 start, Vec2 end, Func<Vec2, bool> walkableFilter)
    {
        Vec2Comparer comparer = new Vec2Comparer();
        var dist = new Dictionary<Vec2, int>(comparer);
        var prev = new Dictionary<Vec2, Vec2?>(comparer);

        var Q = new SortedSet<QueueNode>();

        for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
            {
                var coord = new Vec2(x, y);
                if (walkableFilter(coord) == false)
                {
                    dist[coord] = int.MaxValue;
                }
            }
        }

        dist[start] = 0;
        prev[start] = null;
        Q.Add(new QueueNode(start, 0, 0));

        List<Vec2> neighbours = new List<Vec2>();

        // Search loop
        while (Q.Count > 0)
        {
            var u = Q.Min;
            Q.Remove(Q.Min);

            // Old priority queue value
            if (u.Dist != dist[u.Value])
            {
                continue;
            }

            if (u.Value == end)
            {
                break;
            }

            foreach (var v in GetNeighbours(u.Value, neighbours))
            {
                if (CanEnter(v))
                {
                    int alt = dist[u.Value] + 1;
                    if (dist.TryGetValue(v, out int distInt) == false || alt < dist[v])
                    {
                        dist[v] = alt;
                        bool diagonal = u.Value.x != v.x && u.Value.y != v.y;
                        Q.Add(new QueueNode(v, alt, u.Diagonals + (diagonal ? 1 : 0)));

                        prev[v] = u.Value;
                    }
                }
            }
        }

        // Trace path - if there is one
        var path = new List<Vec2>();

        if (prev.ContainsKey(end))
        {
            Vec2? current = end;

            while (current != null)
            {
                path.Add(current.Value);
                if (prev.ContainsKey(current.Value))
                    current = prev[current.Value];
            }

            path.Reverse();
        }

        return path;
    }

    private static bool CanEnter(Vec2 pos) =>
        Grid[pos.x, pos.y].Exists && Grid[pos.x, pos.y].Locked == false;

    internal static List<Vec2> GetPath(Vec2 origin, Vec2 destination, Person person)
    {
        Person = person;
        InitializeGrid();
        NormalOccupancy(person);

        var otherPath = FindPath(
            new Vec2(origin.x, origin.y),
            new Vec2(destination.x, destination.y),
            CanEnter
        );

        if (otherPath == null)
            return null;

        List<Vec2> path = new List<Vec2>();
        for (int i = 0; i < otherPath.Count(); i++)
        {
            path.Add(otherPath[i]);
        }

        if (path.Count <= 1)
        {
            return null;
        }

        path.RemoveAt(0);

        return path;
    }

    private static void NormalOccupancy(Person person)
    {
        for (int x = 0; x < State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y < State.World.Zones.GetUpperBound(1); y++)
            {
                if (Grid[x, y].Exists)
                    Grid[x, y].Locked = !State.World.Zones[x, y].Accepts(person);
            }
        }
    }

    private static void InitializeGrid()
    {
        if (Initialized == false)
        {
            Grid = new Cell[State.World.Zones.GetLength(0), State.World.Zones.GetLength(1)];
            for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
            {
                for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
                {
                    var zone = State.World.Zones[x, y];
                    Cell cell = new Cell { Exists = zone != null, Locked = false };

                    Grid[x, y] = cell;
                }
            }
            Initialized = true;
            State.World.WorldChanged();
        }
    }

    //static int ComputeHScore(int x, int y, int targetX, int targetY)
    //{
    //    int dx = Mathf.Abs(x - targetX);
    //    int dy = Mathf.Abs(y - targetY);
    //    return Mathf.Max(dx, dy);
    //}

    //static int TotalManhattan(int x, int y, int targetX, int targetY)
    //{
    //    int dx = Mathf.Abs(x - targetX);
    //    int dy = Mathf.Abs(y - targetY);
    //    return (dx + dy);
    //}
}
