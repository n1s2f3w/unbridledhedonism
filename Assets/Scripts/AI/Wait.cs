﻿namespace Assets.Scripts.AI
{
    class Wait : IGoal
    {
        public GoalReturn ExecuteStep()
        {
            return GoalReturn.CompletedGoal;
        }

        public string ReportGoal()
        {
            return "Waiting around";
        }
    }
}
