﻿using OdinSerializer;
using static HelperFunctions;

namespace Assets.Scripts.AI
{
    internal enum GoalType
    {
        None,
        Object,
        Social,
        FreeOralVoredTarget,
        FreeUnbirthedTarget,
        FreeCockVoredTarget,
        FreeAnalVoredTarget,
        //Follow,
        //Vore,
        //Lure,
        //Orgasm
    }

    class InteractWithPerson : IGoal
    {
        [OdinSerialize]
        GoalType Goal;

        [OdinSerialize]
        Person Target;

        [OdinSerialize]
        Person Self;

        [OdinSerialize]
        int GoalPersonLastDistance;

        [OdinSerialize]
        InteractionType DesignatedInteraction;

        public InteractWithPerson(
            GoalType goal,
            Person goalPerson,
            Person self,
            InteractionType designatedInteraction = InteractionType.None
        )
        {
            Goal = goal;
            Target = goalPerson;
            Self = self;
            DesignatedInteraction = designatedInteraction;
            GoalPersonLastDistance =
                1 + Self.Position.GetNumberOfMovesDistance(goalPerson.Position);
        }

        public GoalReturn ExecuteStep()
        {
            if (
                (
                    Target.BeingEaten
                    && (
                        DesignatedInteraction == InteractionType.None
                        || InteractionList.List[DesignatedInteraction].UsedOnPrey == false
                    )
                ) || Target.Dead
            )
                return GoalReturn.AbortGoal;
            if (Self.Position.GetNumberOfMovesDistance(Target.Position) >= GoalPersonLastDistance)
            {
                return GoalReturn.AbortGoal;
            }
            else
            {
                GoalPersonLastDistance = Self.Position.GetNumberOfMovesDistance(Target.Position);
                var destination = Target.Position;
                if (Self.Position == destination)
                {
                    if (
                        Goal == GoalType.FreeCockVoredTarget
                        || Goal == GoalType.FreeOralVoredTarget
                        || Goal == GoalType.FreeUnbirthedTarget
                        || Goal == GoalType.FreeAnalVoredTarget
                    )
                    {
                        if (Target.VoreController.GetLiving(VoreLocation.Any) != null)
                        {
                            InteractionBase interaction = InteractionList.List[
                                InteractionType.FreeStomachPrey
                            ];
                            switch (Goal)
                            {
                                case GoalType.FreeOralVoredTarget:
                                    interaction = InteractionList.List[
                                        InteractionType.FreeStomachPrey
                                    ];
                                    break;
                                case GoalType.FreeUnbirthedTarget:
                                    interaction = InteractionList.List[
                                        InteractionType.FreeWombPrey
                                    ];
                                    break;
                                case GoalType.FreeCockVoredTarget:
                                    interaction = InteractionList.List[
                                        InteractionType.FreeBallsPrey
                                    ];
                                    break;
                                case GoalType.FreeAnalVoredTarget:
                                    interaction = InteractionList.List[
                                        InteractionType.FreeAnalPrey
                                    ];
                                    break;
                            }

                            if (interaction.RunCheck(Self, Target))
                            {
                                return GoalReturn.CompletedGoal;
                            }
                            else
                                return GoalReturn.CompletedGoal;
                        }
                        else
                        {
                            return GoalReturn.AbortGoal;
                        }
                    }
                    else if (Self.IsBusy() == false)
                    {
                        if (DesignatedInteraction != InteractionType.None)
                        {
                            if (
                                DesignatedInteraction == InteractionType.TalkWithPrey
                                && Target.FindMyPredator() == null
                            )
                                DesignatedInteraction = InteractionType.Talk;
                            if (
                                DesignatedInteraction == InteractionType.TauntPrey
                                && Target.FindMyPredator() == null
                            )
                                return GoalReturn.AbortGoal;
                            if (
                                DesignatedInteraction == InteractionType.PlayfulTeasePrey
                                && Target.FindMyPredator() == null
                            )
                                return GoalReturn.AbortGoal;
                            if (
                                DesignatedInteraction == InteractionType.StartSex
                                && (
                                    Target.ActiveSex != null
                                    || Target.VoreController.CurrentSwallow(VoreLocation.Any)
                                        != null
                                )
                            )
                                return GoalReturn.AbortGoal;
                            if (
                                InteractionList.List[DesignatedInteraction].AppearConditional(
                                    Self,
                                    Target
                                ) == false
                            )
                            {
                                UnityEngine.Debug.Log(
                                    $"Canceled impossible action - {Self.FirstName} {DesignatedInteraction}"
                                );
                                return GoalReturn.AbortGoal;
                            }

                            Self.AI.UseInteractionOnTarget(Target, DesignatedInteraction);
                        }
                        else
                        {
                            Self.AI.PickAndDoInteraction(Target);
                        }
                        return GoalReturn.CompletedGoal;
                    }
                }
                if (Self.AI.TryMove(destination))
                    return GoalReturn.DidStep;
                if (CanUseMagic(Self) && Self.Magic.Mana >= 1 && Self.Magic.Duration_Passdoor == 0) // if trymove did not work, cast passdoor and try again
                {
                    SelfActionList.List[SelfActionType.CastPassdoor].OnDo(Self);
                    return GoalReturn.DidStep;
                }
                return GoalReturn.AbortGoal; // Can't reach, either in a locked room or possibly dead
            }
        }

        public string ReportGoal()
        {
            if (DesignatedInteraction != InteractionType.None)
                return $"Going to {Target.FirstName} to perform {DesignatedInteraction}";
            if (
                Goal == GoalType.FreeCockVoredTarget
                || Goal == GoalType.FreeOralVoredTarget
                || Goal == GoalType.FreeUnbirthedTarget
                || Goal == GoalType.FreeAnalVoredTarget
            )
                return $"Going to {Target.FirstName} to free their prey from them";
            return $"Going to {Target.FirstName} to socialize";
        }
    }
}
