﻿using System.Linq;

internal static class TextGenerator
{
    //static readonly List<string> stomachSyn = new List<string>() { "gut", "stomach", "belly", "tummy" };

    internal static string DickSyn => GetRandomStringFrom("dick", "cock", "penis");
    internal static string BreastsSyn => GetRandomStringFrom("boobs", "breasts", "tits");
    internal static string PussySyn => GetRandomStringFrom("slit", "vagina", "pussy", "nethers");
    internal static string PussiesSyn =>
        GetRandomStringFrom("slits", "pussies", "nethers", "nether lips");
    internal static string BallsSyn =>
        GetRandomStringFrom("balls", "scrotum", "testicles", "nuts", "nutsack");
    internal static string ClitSyn => GetRandomStringFrom("clit");
    internal static string ClitsSyn => GetRandomStringFrom("clits");
    internal static string AnusSyn => GetRandomStringFrom("hole", "asshole", "butthole", "rear");
    internal static string AssSyn => GetRandomStringFrom("ass", "butt");
    internal static string BellySyn => GetRandomStringFrom("belly", "gut", "stomach", "tummy");
    internal static string SoftSyn =>
        GetRandomStringFrom("soft", "lush", "plush", "squishy", "cushy");
    internal static string FirmSyn => GetRandomStringFrom("firm", "stiff", "hard", "tight");
    internal static string DigestAdjSyn =>
        GetRandomStringFrom("churning", "gurgling", "greedy", "glorpy", "sloshing");
    internal static string FullAdjSyn =>
        GetRandomStringFrom("prey-stuffed", "distended", "engorged", "taught", "wriggling");

    //internal static string GetRandomStringFrom(List<string> messages)
    //{
    //    if (messages.Count == 0)
    //        return "";
    //    return messages[Rand.Next(messages.Count)];
    //}



    internal static string AOrAn(string word)
    {
        if ("aeiouAEIOU".Contains(word.ToCharArray()[0]))
            return $"an {word}";
        return $"a {word}";
    }

    internal static string GetRandomStringFrom(params string[] messages)
    {
        return messages[Rand.Next(messages.Length)];
    }
}
