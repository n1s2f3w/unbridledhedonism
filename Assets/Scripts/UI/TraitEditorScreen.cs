﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

public class TraitEditorScreen : MonoBehaviour
{
    public Person Person;

    public TMP_Dropdown Quirks;
    public TMP_Dropdown Traits;

    public TextMeshProUGUI QuirkText;
    public TextMeshProUGUI TraitText;

    public Button AddTrait;
    public Button AddRandomTrait;
    public Button RemoveTrait;
    public Button AddQuirk;
    public Button AddRandomQuirk;
    public Button RemoveQuirk;
    public Button AddTraitAll;
    public Button AddQuirkAll;
    public Button RemoveTraitAll;
    public Button RemoveQuirkAll;
    public Button ClearTraits;
    public Button ClearQuirks;

    private void Start()
    {
        AddTrait.onClick.AddListener(TraitAdd);
        AddRandomTrait.onClick.AddListener(TraitAddRandom);
        RemoveTrait.onClick.AddListener(TraitRemove);
        AddQuirk.onClick.AddListener(QuirkAdd);
        AddRandomQuirk.onClick.AddListener(QuirkAddRandom);
        RemoveQuirk.onClick.AddListener(QuirkRemove);
        AddTraitAll.onClick.AddListener(TraitAddAll);
        AddQuirkAll.onClick.AddListener(QuirkAddAll);
        RemoveTraitAll.onClick.AddListener(TraitRemoveAll);
        RemoveQuirkAll.onClick.AddListener(QuirkRemoveAll);
        ClearTraits.onClick.AddListener(TraitClear);
        ClearQuirks.onClick.AddListener(QuirkClear);

        List<string> quirks = new List<string>();
        List<string> traits = new List<string>();

        foreach (Traits type in (Traits[])Enum.GetValues(typeof(Traits)))
        {
            traits.Add(type.ToString());
        }
        foreach (Quirks type in (Quirks[])Enum.GetValues(typeof(Quirks)))
        {
            quirks.Add(type.ToString());
        }
        traits = traits.OrderBy(s => s).ToList();
        quirks = quirks.OrderBy(s => s).ToList();
        Traits.AddOptions(traits);
        Quirks.AddOptions(quirks);

        Traits.RefreshShownValue();
        Quirks.RefreshShownValue();
    }

    public void Randomize()
    {
        Person.Traits = State.World.TraitWeights.AssignTraits(Person);
        Person.Quirks = State.World.TraitWeights.AssignQuirks(Person);
        RefreshText();
    }

    internal void Open(Person person)
    {
        Person = person;
        transform.parent.gameObject.SetActive(true);
        RefreshText();
    }

    void TraitAdd()
    {
        if (Enum.TryParse(Traits.captionText.text, out Traits result))
        {
            Trait trait = TraitList.GetTrait(result);
            foreach (var traitName in Person.Traits.ToList())
            {
                Trait curTrait = TraitList.GetTrait(traitName);
                if (curTrait.Group == trait.Group)
                    Person.Traits.Remove(traitName);
            }
            Person.Traits.Add(result);
        }
        UpdateTraitList();
    }

    void TraitAddRandom()
    {
        State.World.TraitWeights.AddOneRandomTrait(Person);
        UpdateTraitList();
    }

    void TraitRemove()
    {
        if (Enum.TryParse(Traits.captionText.text, out Traits result))
        {
            Person.Traits.Remove(result);
        }
        UpdateTraitList();
    }

    internal void RefreshText()
    {
        UpdateQuirkList();
        UpdateTraitList();
    }

    void UpdateTraitList()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<b>Current Traits:</b>");
        foreach (var trait in Person.Traits)
        {
            sb.AppendLine(trait.ToString());
        }
        TraitText.text = sb.ToString();
    }

    void QuirkAdd()
    {
        if (Enum.TryParse(Quirks.captionText.text, out Quirks result))
        {
            Trait Quirk = TraitList.GetTrait(result);

            foreach (var quirkName in Person.Quirks.ToList())
            {
                Trait curQuirk = TraitList.GetTrait(quirkName);
                if (curQuirk.Group == Quirk.Group)
                    Person.Quirks.Remove(quirkName);
            }
            Person.Quirks.Add(result);
        }
        UpdateQuirkList();
    }

    void QuirkAddRandom()
    {
        State.World.TraitWeights.AddOneRandomQuirk(Person);
        UpdateQuirkList();
    }

    void QuirkRemove()
    {
        if (Enum.TryParse(Quirks.captionText.text, out Quirks result))
        {
            Person.Quirks.Remove(result);
        }
        UpdateQuirkList();
    }

    void UpdateQuirkList()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<b>Current Quirks:</b>");
        foreach (var quirk in Person.Quirks)
        {
            sb.AppendLine(quirk.ToString());
        }
        QuirkText.text = sb.ToString();
    }

    void QuirkClear()
    {
        foreach (var quirkName in Person.Quirks.ToList())
        {
           Person.Quirks.Remove(quirkName);
        }
        UpdateQuirkList();
    }

    void TraitClear()
    {
        foreach (var traitName in Person.Traits.ToList())
        {
            Person.Traits.Remove(traitName);
        }
        UpdateTraitList();
    }

    void QuirkAddAll()
    {
        if (Enum.TryParse(Quirks.captionText.text, out Quirks result))
        {
            foreach (var person in GetEffectivePeople())
            {
                Trait Quirk = TraitList.GetTrait(result);

                foreach (var quirkName in person.Quirks.ToList())
                {
                    Trait curQuirk = TraitList.GetTrait(quirkName);
                    if (curQuirk.Group == Quirk.Group)
                        person.Quirks.Remove(quirkName);
                }
                person.Quirks.Add(result);
            }
        }
        UpdateQuirkList();
    }

    void TraitAddAll()
    {
        if (Enum.TryParse(Traits.captionText.text, out Traits result))
        {
            foreach (var person in GetEffectivePeople())
            {
                Trait trait = TraitList.GetTrait(result);
                foreach (var traitName in person.Traits.ToList())
                {
                    Trait curTrait = TraitList.GetTrait(traitName);
                    if (curTrait.Group == trait.Group)
                        person.Traits.Remove(traitName);
                }
                person.Traits.Add(result);
            }
        }
        UpdateTraitList();
    }

    void QuirkRemoveAll()
    {
        if (Enum.TryParse(Quirks.captionText.text, out Quirks result))
        {
            foreach (var person in GetEffectivePeople())
            {
                Trait Quirk = TraitList.GetTrait(result);

                foreach (var quirkName in person.Quirks.ToList())
                {
                    Trait curQuirk = TraitList.GetTrait(quirkName);
                    if (curQuirk.Group == Quirk.Group)
                        person.Quirks.Remove(quirkName);
                }
            }
        }
        UpdateQuirkList();
    }

    void TraitRemoveAll()
    {
        if (Enum.TryParse(Traits.captionText.text, out Traits result))
        {
            foreach (var person in GetEffectivePeople())
            {
                Trait trait = TraitList.GetTrait(result);
                foreach (var traitName in person.Traits.ToList())
                {
                    Trait curTrait = TraitList.GetTrait(traitName);
                    if (curTrait.Group == trait.Group)
                        person.Traits.Remove(traitName);
                }
            }
        }
        UpdateTraitList();
    }

    List<Person> GetEffectivePeople()
    {
        if (State.GameManager.StartScreen.gameObject.activeSelf)
        {
            List<Person> people = new List<Person>();

            foreach (var obj in State.GameManager.StartScreen.PeopleObjects)
            {
                if (obj.Person != null)
                    people.Add(obj.Person);
            }
            return people;
        }
        else
        {
            return State.World.GetAllPeople();
        }
    }

    public void Close()
    {
        Person.RecalculateBoosts();
        transform.parent.gameObject.SetActive(false);
    }
}
