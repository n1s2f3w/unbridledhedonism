﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class VoreConfiguration : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public Toggle Forced;
    public Toggle Digestion;
    public TMP_Dropdown VoreTypeDropdown;
    public Button ActionButton;

    Person Actor;
    Person Target;

    bool Initialized = false;

    Mode CurrentMode;

    enum Mode
    {
        Standard,
        Sex,
        Prey
    }

    internal void Open(Person actor, Person target)
    {
        if (actor.ActiveSex?.Other == target)
            CurrentMode = Mode.Sex;
        else if (actor.BeingEaten)
            CurrentMode = Mode.Prey;
        else
            CurrentMode = Mode.Standard;
        gameObject.SetActive(true);
        State.GameManager.ActiveInput = true;
        if (Initialized == false)
        {
            var values = Enum.GetValues(typeof(VoreType));
            VoreTypeDropdown.ClearOptions();
            for (int i = 0; i < values.Length; i++)
            {
                VoreTypeDropdown.options.Add(
                    new TMP_Dropdown.OptionData(values.GetValue(i).ToString())
                );
            }
            VoreTypeDropdown.RefreshShownValue();
            Initialized = true;
        }
        Actor = actor;
        Target = target;
        RefreshInfo();
    }

    public void RefreshInfo()
    {
        var interaction = InteractionList.List[GetCorrespondingAction()];
        Enum.TryParse(VoreTypeDropdown.captionText.text, out VoreType result);
        string temp = $"Odds:  {100 * Math.Round(interaction.SuccessOdds(Actor, Target), 2)}%\n";
        ActionButton.interactable = interaction.AppearConditional(Actor, Target);

        if (ActionButton.interactable)
        {
            if (Forced.isOn)
            {
                if (Target.Magic.IsCharmedBy(Actor))
                    temp += "They're charmed by you, so they won't be upset at you for trying.\n";
                else if (Digestion.isOn && Target.IsWillingPreyToTarget(Actor, true))
                    temp += "They're fairly willing, so they won't be upset at you for trying.\n";
                else if (!Digestion.isOn && Target.IsWillingPreyToTarget(Actor))
                    temp += "They're fairly willing, so they won't be upset at you for trying.\n";
                else
                    temp += "This will make them hate you\n";
            }

            if (Digestion.isOn)
            {
                if (
                    Actor.VoreController.HasPrey(result)
                    && Actor.VoreController.PartCurrentlyDigests(result) == false
                )
                {
                    temp +=
                        "This would set you to digest and you currently have prey you aren't digesting, are you sure you want to proceed?";
                }
            }
            else
            {
                if (
                    Actor.VoreController.HasPrey(result)
                    && Actor.VoreController.PartCurrentlyDigests(result)
                )
                {
                    temp +=
                        "This would set you to not digest and you currently have prey you're digesting, continue anyway?";
                }
            }
        }
        else
        {
            if (Digestion.isOn)
                temp += Actor.VoreController.GetReason(Target, result, DigestionAlias.CanVore);
            else
                temp += Actor.VoreController.GetReason(Target, result, DigestionAlias.CanEndo);
        }

        ActionButton.interactable = interaction.AppearConditional(Actor, Target);
        Text.text = temp;
    }

    InteractionType GetCorrespondingAction()
    {
        //May need to replace this with better logic later
        if (CurrentMode != Mode.Prey)
        {
            if (VoreTypeDropdown.value == 0)
            {
                if (Forced.isOn)
                    return InteractionType.OralVore;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.AskToOralVoreDigest;
                    else
                        return InteractionType.AskToOralVore;
                }
            }
            if (VoreTypeDropdown.value == 1)
            {
                if (Forced.isOn)
                    return InteractionType.Unbirth;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.AskToUnbirthDigest;
                    else
                        return InteractionType.AskToUnbirth;
                }
            }
            if (VoreTypeDropdown.value == 2)
            {
                if (Forced.isOn)
                    return InteractionType.CockVore;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.AskToCockVoreDigest;
                    else
                        return InteractionType.AskToCockVore;
                }
            }
            if (VoreTypeDropdown.value == 3)
            {
                if (Forced.isOn)
                    return InteractionType.AnalVore;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.AskToAnalVoreDigest;
                    else
                        return InteractionType.AskToAnalVore;
                }
            }
        }
        else if (CurrentMode == Mode.Prey)
        {
            if (VoreTypeDropdown.value == 0)
            {
                if (Forced.isOn)
                    return InteractionType.PreyEatOtherPrey;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.PreyAskToOralVoreDigest;
                    else
                        return InteractionType.PreyAskToOralVore;
                }
            }
            if (VoreTypeDropdown.value == 1)
            {
                if (Forced.isOn)
                    return InteractionType.PreyUnbirthOtherPrey;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.PreyAskToUnbirthDigest;
                    else
                        return InteractionType.PreyAskToUnbirth;
                }
            }
            if (VoreTypeDropdown.value == 2)
            {
                if (Forced.isOn)
                    return InteractionType.PreyCockVoreOtherPrey;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.PreyAskToCockVoreDigest;
                    else
                        return InteractionType.PreyAskToCockVore;
                }
            }
            if (VoreTypeDropdown.value == 3)
            {
                if (Forced.isOn)
                    return InteractionType.PreyAnalVoreOtherPrey;
                else
                {
                    if (Digestion.isOn)
                        return InteractionType.PreyAskToAnalVoreDigest;
                    else
                        return InteractionType.PreyAskToAnalVore;
                }
            }
        }

        Debug.LogWarning("Invalid Combo");
        return InteractionType.OralVore;
    }

    SexInteractionType GetCorrespondingActionSex()
    {
        //May need to replace this with better logic later

        if (VoreTypeDropdown.value == 0)
        {
            if (Forced.isOn)
                return SexInteractionType.KissVore;
            else
            {
                if (Digestion.isOn)
                    return SexInteractionType.SexAskToOralVoreDigest;
                else
                    return SexInteractionType.SexAskToOralVore;
            }
        }
        if (VoreTypeDropdown.value == 1)
        {
            if (Forced.isOn)
                return SexInteractionType.SexUnbirth;
            else
            {
                if (Digestion.isOn)
                    return SexInteractionType.SexAskToUnbirthDigest;
                else
                    return SexInteractionType.SexAskToUnbirth;
            }
        }
        if (VoreTypeDropdown.value == 2)
        {
            if (Forced.isOn)
                return SexInteractionType.SexCockVore;
            else
            {
                if (Digestion.isOn)
                    return SexInteractionType.SexAskToCockVoreDigest;
                else
                    return SexInteractionType.SexAskToCockVore;
            }
        }
        if (VoreTypeDropdown.value == 3)
        {
            if (Forced.isOn)
                return SexInteractionType.SexAnalVore;
            else
            {
                if (Digestion.isOn)
                    return SexInteractionType.SexAskToAnalVoreDigest;
                else
                    return SexInteractionType.SexAskToAnalVore;
            }
        }

        Debug.LogWarning("Invalid Combo");
        return SexInteractionType.KissVore;
    }

    public void DoAction()
    {
        Enum.TryParse(VoreTypeDropdown.captionText.text, out VoreType result);
        Actor.VoreController.SetPartDigest(result, Digestion.isOn);
        if (CurrentMode != Mode.Sex)
        {
            var interaction = InteractionList.List[GetCorrespondingAction()];
            interaction.RunCheck(Actor, Target);
        }
        else
        {
            var interaction = SexInteractionList.List[GetCorrespondingActionSex()];
            interaction.OnDo(Actor, Target);
        }
        State.World.NextTurn();
        gameObject.SetActive(false);
        State.GameManager.ActiveInput = false;
    }

    public void CancelAction()
    {
        gameObject.SetActive(false);
        State.GameManager.ActiveInput = false;
    }
}
