﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SaveLoadInfo : MonoBehaviour
{
    public Button DeleteSave;
    public Button SaveGame;
    public Button LoadGame;
    public Text LeftText;
    public Text RightText;
    public TMP_InputField SavedGameName;
}
