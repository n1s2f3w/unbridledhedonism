﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    public TextMeshProUGUI InfoText;
    public Button Button;

    public void Update()
    {
        if (!gameObject.activeSelf)
            return;
        if (State.KeyManager.SubmitPressed || State.KeyManager.CancelPressed)
            Button.onClick.Invoke();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
