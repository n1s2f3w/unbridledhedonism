﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class SavedCharacterScreen : MonoBehaviour
    {
        SavedPersonContainer Container;

        internal List<SavedPerson> Specials;

        public List<SavedPersonUI> PeopleObjects;

        public TMP_InputField TagSearch;

        public GameObject PeopleObjectPrefab;
        public Transform PeopleObjectFolder;

        bool DirtyTags;

        internal void Open()
        {
            Container = State.SavedPersonController.Container;
            gameObject.SetActive(true);
            foreach (
                var person in State.SavedPersonController.Specials
                    .OrderBy(s => s.LastName)
                    .ThenBy(s => s.FirstName)
            )
            {
                var obj = Instantiate(PeopleObjectPrefab, PeopleObjectFolder)
                    .GetComponent<SavedPersonUI>();
                PeopleObjects.Add(obj);
                obj.AddToGame.onClick.AddListener(() => LoadCharacter(person));
                obj.AddToGame.onClick.AddListener(() => obj.AddToGame.interactable = false);
                obj.Tags.text = person.Tags;
                obj.Tags.onValueChanged.AddListener((s) => ChangeTag(s, person));
                obj.DeleteCharacter.GetComponentInChildren<TextMeshProUGUI>().text =
                    "Hide Character";
                obj.DeleteCharacter.onClick.AddListener(() => HideCharacter(obj, person));
                if (
                    State.SavedPersonController.Container.HiddenList.Contains(
                        person.FirstName + person.LastName
                    )
                )
                    obj.gameObject.SetActive(false);
                obj.Name.text = $"<color=#ff881a>{person.FirstName} {person.LastName}</color>";
                obj.AddToGame.interactable =
                    State.GameManager.StartScreen.PeopleObjects
                        .Where(
                            (s) =>
                                s.FirstName.text == person.FirstName
                                && s.LastName.text == person.LastName
                        )
                        .Any() == false;
            }
            foreach (var person in Container.List.OrderBy(s => s.LastName).ThenBy(s => s.FirstName))
            {
                var obj = Instantiate(PeopleObjectPrefab, PeopleObjectFolder)
                    .GetComponent<SavedPersonUI>();
                PeopleObjects.Add(obj);
                obj.AddToGame.onClick.AddListener(() => LoadCharacter(person));
                obj.AddToGame.onClick.AddListener(() => obj.AddToGame.interactable = false);
                obj.Tags.text = person.Tags;
                obj.Tags.onValueChanged.AddListener((s) => ChangeTag(s, person));
                obj.DeleteCharacter.onClick.AddListener(() => DeleteCharacter(obj, person));
                obj.Name.text = $"{person.FirstName} {person.LastName}";
                obj.AddToGame.interactable =
                    State.GameManager.StartScreen.PeopleObjects
                        .Where(
                            (s) =>
                                s.FirstName.text == person.FirstName
                                && s.LastName.text == person.LastName
                        )
                        .Any() == false;
            }
        }

        void ChangeTag(string s, SavedPerson person)
        {
            person.Tags = s;
            DirtyTags = true;
        }

        public void UnHideAll()
        {
            foreach (var obj in PeopleObjects)
            {
                obj.gameObject.SetActive(true);
            }
            State.SavedPersonController.Container.HiddenList.Clear();
            SaveToFile();
        }

        public void AddCharactersMatchingTags()
        {
            foreach (var obj in PeopleObjects)
            {
                if (obj.AddToGame.interactable && obj.Tags.text.Contains(TagSearch.text))
                    obj.AddToGame.onClick.Invoke();
            }
        }

        public void CleanUp()
        {
            if (DirtyTags)
            {
                SaveToFile();
                DirtyTags = false;
            }
            gameObject.SetActive(false);
            foreach (var obj in PeopleObjects.ToList())
            {
                PeopleObjects.Remove(obj);
                Destroy(obj.gameObject);
            }
        }

        internal void DeleteCharacter(SavedPersonUI obj, SavedPerson person)
        {
            PeopleObjects.Remove(obj);
            Destroy(obj.gameObject);
            State.SavedPersonController.Container.List.Remove(person);
            SaveToFile();
        }

        internal void HideCharacter(SavedPersonUI obj, SavedPerson person)
        {
            obj.gameObject.SetActive(false);
            State.SavedPersonController.Container.HiddenList.Add(
                person.FirstName + person.LastName
            );
            SaveToFile();
        }

        internal void SaveToFile()
        {
            string filename = Path.Combine(State.StorageDirectory, "SavedChars.dat");
            Container.LastSavedVersion = State.Version;
            try
            {
                byte[] bytes = SerializationUtility.SerializeValue(Container, DataFormat.Binary);
                File.WriteAllBytes(filename, bytes);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                State.GameManager.CreateMessageBox("Couldn't save saved characters");
            }
        }

        internal void SaveCharacter(StartPersonUI ui)
        {
            if (Container == null)
                Container = State.SavedPersonController.Container;
            SavedPerson person = new SavedPerson();
            person.FirstName = ui.FirstName.text;
            person.LastName = ui.LastName.text;
            person.Gender = ui.Gender.value;
            person.Orientation = ui.Orientation.value;
            person.Personality = ui.Personality.value;
            person.Race = ui.Race.captionText.text;
            person.CanVore = ui.CanVore.isOn;
            person.CustomAppearance = ui.CustomAppearance;
            person.CustomPersonality = ui.CustomPersonality;

            person.SavedVersion = State.Version;

            person.Tags = ui.Tags;

            person.Person = ui.Person;

            if (person.Person != null)
                person.CustomPersonality = null;

            if (person.Person != null)
                person.Person.Purify();

            try
            {
                string filename = Path.Combine(Application.persistentDataPath, "LastSavedChar.txt");
                byte[] bytes = SerializationUtility.SerializeValue(person, DataFormat.JSON);
                File.WriteAllBytes(filename, bytes);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            var old = Container.List.RemoveAll(
                s => s.FirstName == person.FirstName && s.LastName == person.LastName
            );
            Container.List.Add(person);

            SaveToFile();
        }

        internal void LoadCharacter(SavedPerson person)
        {
            State.GameManager.StartScreen.AddPerson(person);
        }
    }

    class SavedPersonContainer
    {
        [OdinSerialize]
        internal List<SavedPerson> List;

        [OdinSerialize]
        internal List<string> HiddenList;

        [OdinSerialize]
        internal string LastSavedVersion;
    }

    class SavedPerson
    {
        [OdinSerialize]
        internal Personality CustomPersonality;

        [OdinSerialize]
        internal PartList CustomAppearance;

        [OdinSerialize]
        internal Person Person;

        [OdinSerialize]
        internal string SavedVersion;

        [OdinSerialize]
        public string Tags;

        [OdinSerialize]
        public string FirstName;

        [OdinSerialize]
        public string LastName;

        [OdinSerialize]
        public int Gender;

        [OdinSerialize]
        public string Race;

        [OdinSerialize]
        public int Orientation;

        [OdinSerialize]
        public int Personality;

        [OdinSerialize]
        public bool CanVore;
    }
}
