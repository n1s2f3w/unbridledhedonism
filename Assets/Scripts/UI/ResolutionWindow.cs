﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class ResolutionWindow : MonoBehaviour
{
    private const string RESOLUTION_PREF_KEY = "resolution";

    public TextMeshProUGUI resolutionText;

    public Toggle FullScreen;

    private Resolution[] resolutions;

    private int currentResolutionIndex = 0;

    void Start()
    {
        resolutions = Screen.resolutions;

        currentResolutionIndex = PlayerPrefs.GetInt(RESOLUTION_PREF_KEY, 0);

        if (resolutions.Length == 0)
        {
            State.GameManager.CreateMessageBox(
                "Couldn't retrieve monitor supported resolutions, you'll be unable to change"
            );
            return;
        }
        if (currentResolutionIndex >= resolutions.Length)
        {
            currentResolutionIndex = resolutions.Length - 1;
        }

        SetResolutionText(resolutions[currentResolutionIndex]);
    }

    private int GetNextWrappedIndex<T>(IList<T> collection, int currentIndex)
    {
        if (collection.Count < 1)
            return 0;
        return (currentIndex + 1) % collection.Count;
    }

    private int GetPreviousWrappedIndex<T>(IList<T> collection, int currentIndex)
    {
        if (collection.Count < 1)
            return 0;
        if ((currentIndex - 1) < 0)
            return collection.Count - 1;
        return (currentIndex - 1) % collection.Count;
    }

    private void SetResolutionText(Resolution resolution)
    {
        resolutionText.text = $"{resolution.width} x {resolution.height}";
    }

    public void SetNextResolution()
    {
        int prevWidth = resolutions[currentResolutionIndex].width;
        int prevHeight = resolutions[currentResolutionIndex].height;
        for (int i = 0; i < 5; i++)
        {
            currentResolutionIndex = GetNextWrappedIndex(resolutions, currentResolutionIndex);
            if (
                prevHeight == resolutions[currentResolutionIndex].height
                && prevWidth == resolutions[currentResolutionIndex].width
            )
                continue;
            break;
        }

        SetResolutionText(resolutions[currentResolutionIndex]);
    }

    public void SetPreviousResolution()
    {
        int prevWidth = resolutions[currentResolutionIndex].width;
        int prevHeight = resolutions[currentResolutionIndex].height;
        for (int i = 0; i < 5; i++)
        {
            currentResolutionIndex = GetPreviousWrappedIndex(resolutions, currentResolutionIndex);
            if (
                prevHeight == resolutions[currentResolutionIndex].height
                && prevWidth == resolutions[currentResolutionIndex].width
            )
                continue;
            break;
        }
        SetResolutionText(resolutions[currentResolutionIndex]);
    }

    private void SetAndApplyResolution(int newResoltionIndex)
    {
        if (resolutions.Length <= newResoltionIndex)
        {
            if (resolutions.Length > 0)
            {
                currentResolutionIndex = 0;
                ApplyResolution(resolutions[0]);
            }
            return;
        }
        currentResolutionIndex = newResoltionIndex;
        ApplyResolution(resolutions[currentResolutionIndex]);
    }

    private void ApplyResolution(Resolution resolution)
    {
        SetResolutionText(resolution);

        Screen.SetResolution(
            resolution.width,
            resolution.height,
            FullScreen.isOn ? FullScreenMode.FullScreenWindow : FullScreenMode.Windowed
        );
        PlayerPrefs.SetInt(RESOLUTION_PREF_KEY, currentResolutionIndex);
    }

    public void ApplyChanges()
    {
        SetAndApplyResolution(currentResolutionIndex);
    }
}
