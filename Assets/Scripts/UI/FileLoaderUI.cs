﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class FileLoaderUI : MonoBehaviour
{
    public GameObject ButtonType;
    public Transform Folder;

    internal void CreateMapLoadButton(string file)
    {
        var button = Instantiate(ButtonType, Folder);
        button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text =
            $"Open File : {Path.GetFileName(file)}";
        button.GetComponent<Button>().onClick.AddListener(() => State.Load(file, true));
        button
            .GetComponent<Button>()
            .onClick.AddListener(() => State.GameManager.MapEditor.ClearUndo());
        button.GetComponent<Button>().onClick.AddListener(() => TerminateSelf());
    }

    public void TerminateSelf()
    {
        Destroy(gameObject);
    }
}
