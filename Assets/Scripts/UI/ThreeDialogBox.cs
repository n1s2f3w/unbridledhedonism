﻿using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class ThreeDialogBox : MonoBehaviour
{
    Action YesAction;
    Action NoAction;
    Action AIAction;
    public Button Yes;
    public Button No;
    public Button AI;
    public TextMeshProUGUI Text;

    public TextMeshProUGUI SubmitKey;
    public TextMeshProUGUI CancelKey;
    public TextMeshProUGUI AIKey;

    private void Update()
    {
        if (State.KeyManager.SubmitPressed)
            YesClicked();
        else if (State.KeyManager.CancelPressed)
            NoClicked();
        else if (State.KeyManager.AIControlPressed)
            AIClicked();
    }

    public void SetData(
        Action action,
        string yesText,
        string noText,
        string aiText,
        string mainText,
        Action noAction = null,
        Action aiAction = null
    )
    {
        YesAction = action;
        Yes.GetComponentInChildren<Text>().text = yesText;
        No.GetComponentInChildren<Text>().text = noText;
        AI.GetComponentInChildren<Text>().text = aiText;
        Text.text = mainText;
        NoAction = noAction;
        AIAction = aiAction;
        SubmitKey.text = State.KeyManager.SubmitKey;
        CancelKey.text = State.KeyManager.CancelKey;
        AIKey.text = State.KeyManager.AIControlKey;
    }

    public void YesClicked()
    {
        YesAction?.Invoke();
        Destroy(gameObject);
    }

    public void NoClicked()
    {
        NoAction?.Invoke();
        Destroy(gameObject);
    }

    public void AIClicked()
    {
        AIAction?.Invoke();
        Destroy(gameObject);
    }
}
