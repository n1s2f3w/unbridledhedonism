﻿using System;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SaveLoadScreen : MonoBehaviour
{
    public SaveLoadInfo SaveInfo;
    public Transform SaveNamesFolder;
    public GameObject DefaultButton;

    /// The save path for the currently selected or user-provided save name.
    internal string ActiveSavePath
    {
        get { return Path.Combine(State.SaveDirectory, $"{SaveInfo.SavedGameName.text}.sav"); }
    }

    public void InputChanged()
    {
        SaveInfo.LoadGame.interactable = false;
        SaveInfo.DeleteSave.interactable = false;
        try
        {
            if (!File.Exists(ActiveSavePath))
            {
                SaveInfo.LeftText.text = "No file with this name";
                SaveInfo.RightText.text = "";
                SaveInfo.LoadGame.interactable = false;
                return;
            }
            SaveInfo.DeleteSave.interactable = true;
            World TempWorld = State.PreviewSave(ActiveSavePath);
            if (string.Compare(TempWorld.SaveVersion, "5") < 0)
            {
                SaveInfo.LeftText.text =
                    "Can't load that save as it's from before version 5 (there were many changes to the way things were saved, I opted to do a bunch at once so that it's less likely to happen in the future.)";
                SaveInfo.LoadGame.interactable = false;
                return;
            }
            if (TempWorld == null)
            {
                SaveInfo.LeftText.text = "Invalid save (from a incompatible version?)";
                SaveInfo.LoadGame.interactable = false;
                return;
            }
            SaveInfo.LoadGame.interactable = true;

            StringBuilder sbLeft = new StringBuilder();
            StringBuilder sbRight = new StringBuilder();

            int count = 0;
            foreach (Person person in TempWorld.GetPeople(true))
            {
                count++;
                if (count < 9)
                    sbLeft.AppendLine($"{person.GetFullName()}");
            }
            if (count >= 9)
                sbLeft.AppendLine($"plus {count - 8} others");

            sbRight.AppendLine($"Save Version: {TempWorld.SaveVersion}");
            sbRight.AppendLine($"Saved date: {File.GetLastWriteTime(ActiveSavePath)}");
            sbRight.AppendLine($"Turn: {TempWorld.Turn}");
            sbRight.AppendLine($"Controlled Player: {TempWorld.ControlledPerson.GetFullName()}");
            if (string.IsNullOrWhiteSpace(TempWorld.MapName) == false)
                sbRight.AppendLine($"Map: {TempWorld.MapName}");
            sbRight.AppendLine(
                $"Map Size: {TempWorld.Zones.GetLength(0)} x {TempWorld.Zones.GetLength(1)}"
            );
            sbRight.AppendLine($"Resurrection: {(TempWorld.Settings.NursesActive ? "On" : "Off")}");

            SaveInfo.LeftText.text = sbLeft.ToString();
            SaveInfo.RightText.text = sbRight.ToString();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            SaveInfo.LeftText.text = "Error previewing the save file";
        }
    }

    public void Open()
    {
        gameObject.SetActive(true);
        ListSlots();
    }

    public void Save()
    {
        State.Save(ActiveSavePath);
        gameObject.SetActive(false);
        State.GameManager.MenuScreen.gameObject.SetActive(false);
        ListSlots();
    }

    public void Load()
    {
        State.Load(ActiveSavePath);
        gameObject.SetActive(false);
        State.GameManager.MenuScreen.gameObject.SetActive(false);
        State.GameManager.TitleScreen.gameObject.SetActive(false);
    }

    public void DeleteSave()
    {
        var box = State.GameManager.CreateDialogBox();
        box.SetData(
            ActuallyDelete,
            "Delete",
            "Cancel",
            "Are you sure you want to delete this saved game?"
        );
    }

    void ActuallyDelete()
    {
        if (File.Exists(ActiveSavePath))
            File.Delete(ActiveSavePath);
        InputChanged();
        ListSlots();
    }

    internal void ListSlots()
    {
        int children = SaveNamesFolder.childCount;
        for (int i = children - 1; i >= 0; i--)
        {
            Destroy(SaveNamesFolder.GetChild(i).gameObject);
        }
        BuildFiles(State.SaveDirectory, "sav");
        bool InMainMenu = State.World == null;
        SaveInfo.SaveGame.interactable = !InMainMenu;
        InputChanged();
    }

    private void BuildFiles(string directory, string extension)
    {
        if (Directory.Exists(directory) == false)
            Directory.CreateDirectory(directory);
        string[] files = Directory.GetFiles(directory);

        foreach (string file in files)
        {
            if (!File.Exists(file))
                continue;

            if (CompatibleFileExtension(file, extension))
            {
                var filename = Path.GetFileNameWithoutExtension(file);

                var button = Instantiate(DefaultButton, SaveNamesFolder).GetComponent<Button>();
                button.GetComponentInChildren<TextMeshProUGUI>().text = $"{filename}";
                button
                    .GetComponent<Button>()
                    .onClick.AddListener(() => SaveInfo.SavedGameName.text = filename);
            }
        }
    }

    public bool CompatibleFileExtension(string file, string extension)
    {
        if (extension.Length == 0)
        {
            return true;
        }

        if (file.EndsWith("." + extension))
        {
            return true;
        }

        // Not found, return not compatible
        return false;
    }
}
