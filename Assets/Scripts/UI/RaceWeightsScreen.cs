﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System.Collections.Generic;

public class RaceWeightsScreen : MonoBehaviour
{
    public GameObject Slider;

    public Transform Folder;

    public void Open()
    {
        Open(null);
    }

    internal void Open(RaceWeights weights)
    {
        if (weights == null)
        {
            weights = State.GameManager.StartScreen.RaceWeights;
        }

        List<string> needsRemoval = new List<string>();
        foreach (var kvp in weights.Weights)
        {
            if (kvp.Key != "Human" && RaceManager.GetRace(kvp.Key).Name == "Human") //Did it return the default race?
            {
                needsRemoval.Add(kvp.Key);
            }
        }
        foreach (var removeRace in needsRemoval)
        {
            weights.Weights.Remove(removeRace);
        }

        foreach (var race in RaceManager.PickableRaces)
        {
            if (weights.Weights.ContainsKey(race) == false)
            {
                weights.Weights[race] = 500;
            }
        }

        foreach (var weight in weights.Weights.OrderBy(s => s.Key))
        {
            var obj = Instantiate(Slider, Folder);
            obj.name = weight.Key;
            var slider = obj.GetComponentInChildren<Slider>();
            slider.minValue = 0;
            slider.maxValue = 1000;
            slider.wholeNumbers = true;
            slider.value = weight.Value;
            obj.GetComponentInChildren<TextMeshProUGUI>().text = $"{weight.Key}";
        }

        gameObject.SetActive(true);
    }

    void Close()
    {
        gameObject.SetActive(false);
        int children = Folder.childCount;
        for (int i = children - 1; i >= 0; i--)
        {
            Destroy(Folder.GetChild(i).gameObject);
        }
        if (State.GameManager.StartScreen.gameObject.activeSelf)
            State.GameManager.StartScreen.RefreshAllSexes();
    }

    public void CloseWithoutSave()
    {
        Close();
    }

    public void CloseAndSave()
    {
        RaceWeights weights;
        if (State.GameManager.StartScreen.gameObject.activeSelf)
            weights = State.GameManager.StartScreen.RaceWeights;
        else
            weights = State.World.RaceWeights;
        for (int i = 0; i < Folder.childCount; i++)
        {
            weights.Weights[Folder.GetChild(i).name] = (int)
                Folder.GetChild(i).GetComponentInChildren<Slider>().value;
        }
        Close();
    }

    public void CloseSaveAndSetDefault()
    {
        CloseAndSave();
        if (State.GameManager.StartScreen.gameObject.activeSelf)
            State.WriteToFile(State.GameManager.StartScreen.RaceWeights, "RaceWeights.dat");
        else
            State.WriteToFile(State.World.RaceWeights, "RaceWeights.dat");
    }
}
