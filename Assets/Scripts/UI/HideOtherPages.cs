using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOtherPages : MonoBehaviour
{
    public Transform Pages;
    public void Hide(string selfName)
    {
        foreach (Transform child in Pages)
        {
            if (child.name == selfName)
            {
                child.gameObject.SetActive(true);
            }else{
                child.gameObject.SetActive(false);
            }
        }
    }
}
