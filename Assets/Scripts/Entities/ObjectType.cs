﻿public enum ObjectType
{
    None,
    Bed,
    Clothes,
    Food,
    Shower,
    Bathroom,
    Dumpster,
    NurseOffice,
    Gym,
    Library,
}
