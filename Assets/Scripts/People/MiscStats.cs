﻿using OdinSerializer;
using System.Collections.Generic;

namespace Assets.Scripts.People
{
    public class MiscStats
    {
        [OdinSerialize]
        [VariableEditorIgnores]
        public int TurnAdded;

        [OdinSerialize]
        [VariableEditorIgnores]
        public int TurnsAlive;

        // WEIGHT GAIN
        [OdinSerialize, ProperName("Current Weight Gained")]
        [Description("The % change in weight from vore over this character's lifetime")]
        [FloatRange(0, 999)]
        public float CurrentWeightGain;

        [OdinSerialize, ProperName("Current Breast Gained")]
        [Description("The % change in breast size from vore over this character's lifetime")]
        [FloatRange(0, 999)]
        public float CurrentBreastGain;

        [OdinSerialize, ProperName("Current Height Gained")]
        [Description("The % change in height from vore over this character's lifetime")]
        [FloatRange(0, 999)]
        public float CurrentHeightGain;

        [OdinSerialize, ProperName("Current Dick Gained")]
        [Description("The % change in dick size from vore over this character's lifetime")]
        [FloatRange(0, 999)]
        public float CurrentDickGain;

        [OdinSerialize, ProperName("Total Weight Gained")]
        [Description("The total amount of weight gained over the lifetime of this character")]
        [FloatRange(0, 999)]
        public float TotalWeightGain;

        [OdinSerialize, ProperName("Total Weight Lost")]
        [Description("The total amount of weight lost over the lifetime of this character")]
        [FloatRange(0, 999)]
        public float TotalWeightLoss;

        [OdinSerialize, ProperName("Times Swallowed Other")]
        [Description("The number of times this character has started swallowing someone else")]
        [IntegerRange(0, 999)]
        public int TimesSwallowedOtherStart;

        [OdinSerialize, ProperName("Times Unbirthed Other")]
        [Description("The number of times this character has started unbirthing someone else")]
        [IntegerRange(0, 999)]
        public int TimesUnbirthedOtherStart;

        [OdinSerialize, ProperName("Times Anal Vored Other")]
        [Description("The number of times this character has started anal voring someone else")]
        [IntegerRange(0, 999)]
        public int TimesAnalVoredOtherStart;

        [OdinSerialize, ProperName("Times Cock Vored Other")]
        [Description("The number of times this character has started cock voring someone else")]
        [IntegerRange(0, 999)]
        public int TimesCockVoredOtherStart;

        [OdinSerialize, ProperName("Times Been Swallowed")]
        [Description("The number of times this character has started being swallowed")]
        [IntegerRange(0, 999)]
        public int TimesBeenSwallowedStart;

        [OdinSerialize, ProperName("Times Been Unbirthed")]
        [Description("The number of times this character has started being unbirthed")]
        [IntegerRange(0, 999)]
        public int TimesBeenUnbirthedStart;

        [OdinSerialize, ProperName("Times Been Anal Vored")]
        [Description("The number of times this character has started being anal vored")]
        [IntegerRange(0, 999)]
        public int TimesBeenAnalVoredStart;

        [OdinSerialize, ProperName("Times Been Cock Vored")]
        [Description("The number of times this character has started being cock vored")]
        [IntegerRange(0, 999)]
        public int TimesBeenCockVoredStart;

        [OdinSerialize, ProperName("Times Swallowed Other")]
        [Description(
            "The number of times this character has swallowed someone else.  Only counts if the consuming phase wasn't interrupted."
        )]
        [IntegerRange(0, 999)]
        public int TimesSwallowedOther;

        [OdinSerialize, ProperName("Times Unbirthed Other")]
        [Description("The number of times this character has unbirthed someone else")]
        [IntegerRange(0, 999)]
        public int TimesUnbirthedOther;

        [OdinSerialize, ProperName("Times Anal Vored Other")]
        [Description("The number of times this character has anal vored someone else")]
        [IntegerRange(0, 999)]
        public int TimesAnalVoredOther;

        [OdinSerialize, ProperName("Times Cock Vored Other")]
        [Description("The number of times this character has cock vored someone else")]
        [IntegerRange(0, 999)]
        public int TimesCockVoredOther;

        [OdinSerialize, ProperName("Times Been Swallowed")]
        [Description("The number of times this character has been swallowed")]
        [IntegerRange(0, 999)]
        public int TimesBeenSwallowed;

        [OdinSerialize, ProperName("Times Been Unbirthed")]
        [Description("The number of times this character has been unbirthed")]
        [IntegerRange(0, 999)]
        public int TimesBeenUnbirthed;

        [OdinSerialize, ProperName("Times Been Anal Vored")]
        [Description("The number of times this character has been anal vored")]
        [IntegerRange(0, 999)]
        public int TimesBeenAnalVored;

        [OdinSerialize, ProperName("Times Been Cock Vored")]
        [Description("The number of times this character has been cock vored")]
        [IntegerRange(0, 999)]
        public int TimesBeenCockVored;

        [OdinSerialize, ProperName("Times Been Predator")]
        [Description("The number of times this character has been predator")]
        [IntegerRange(0, 999)]
        public int TimesBeenPredator;

        [OdinSerialize, ProperName("Times Been Prey")]
        [Description("The number of times this character has been prey")]
        [IntegerRange(0, 999)]
        public int TimesBeenPrey;

        [OdinSerialize, ProperName("Times Digested Other")]
        [Description("The number of times this character has digested or melted someone else.")]
        [IntegerRange(0, 999)]
        public int TimesDigestedOther;

        [OdinSerialize, ProperName("Times Been Digested")]
        [Description(
            "The number of times this character has been digested or melted.  (only applies for reformation, as anything else gets rid of the character)"
        )]
        [IntegerRange(0, 999)]
        public int TimesBeenDigested;

        [OdinSerialize, ProperName("Times Had Sex")]
        [Description(
            "The number of times this character has had sex."
        )]
        [IntegerRange(0, 999)]
        public int TimesHadSex;

        [OdinSerialize, ProperName("Times Had Orgasm")]
        [Description(
            "The number of times this character has had an orgasm."
        )]
        [IntegerRange(0, 999)]
        public int TimesHadOrgasm;

        [OdinSerialize, ProperName("Unique Sex Partners")]
        [Description(
            "The list of people this character has had sex with."
        )]
        internal List<Person> UniqueSexPartners = new List<Person>();

        [OdinSerialize, ProperName("Unique Predators")]
        [Description(
            "The number of people this character has been eaten by."
        )]
        internal List<Person> UniquePredators = new List<Person>();

        [OdinSerialize, ProperName("Unique Prey")]
        [Description(
            "The number of people this character has eaten."
        )]
        internal List<Person> UniquePrey = new List<Person>();
    }
}