﻿using OdinSerializer;

class DisposalData
{
    [OdinSerialize]
    internal Person Person;

    [OdinSerialize]
    internal int TurnCreated;

    [OdinSerialize]
    internal int TurnsDone;

    [OdinSerialize]
    internal VoreLocation Location;

    public DisposalData(Person person, int turnCreated, VoreLocation location)
    {
        Person = person;
        TurnCreated = turnCreated;
        Location = location;
    }
}
