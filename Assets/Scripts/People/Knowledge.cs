﻿using OdinSerializer;
using System.Collections.Generic;
using System.Linq;

public class Knowledge
{
    [OdinSerialize]
    public bool IsDating;

    [OdinSerialize]
    public Person DatingTarget;

    [OdinSerialize]
    public bool KnowsOrientation;

    [OdinSerialize]
    public bool KnowsDigestionImmune;

    [OdinSerialize]
    internal List<VoreProgress> SeenVore;

    public Knowledge()
    {
        IsDating = false;
        KnowsOrientation = false;
        KnowsDigestionImmune = false;
        DatingTarget = null;
        SeenVore = new List<VoreProgress>();
    }

    internal void ClearUseless()
    {
        foreach (var vore in SeenVore.ToList())
        {
            if (vore.Target.FindMyPredator() == null)
                SeenVore.Remove(vore);
        }
    }
}
