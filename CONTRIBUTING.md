# Setting up.

* Install a Git client such as [Git for Windows](https://gitforwindows.org/).
* Install [Unity Hub](https://unity.com/unity-hub).
* Install [Visual Studio Code](https://code.visualstudio.com/) if you plan on editing code, CSV, or text files.
* Clone the repo.
  With Git for Windows installed you can right click on the folder you plan on storing your Git projects in then pick 'Git Bash Here'.
  In this new terminal you should run the following command:
  `git clone https://gitlab.com/Zebranky/unbridledhedonism.git`
* Open the cloned repo with Unity Hub and follow any instructions presented.
